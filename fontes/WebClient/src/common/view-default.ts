export class ViewDefault {

    private errorMessage: string;
    private successMessage: string;

    constructor(){
        this.clearMessage();
    }

    setErrorMessageDefault(){
        this.setErrorMessage("Ocorreu um erro :'(");
    }

    setErrorMessage(message: string){
        this.errorMessage = message;
        this.successMessage = "";
    }

    setSuccessMessage(message: string){
        this.successMessage = message;
        this.errorMessage = "";    
    }

    clearMessage(){
        this.successMessage = "";
        this.errorMessage = "";
    }

    getImageBase64(imageType: string, image: string){
        return 'data:' + imageType + ';base64,' + image;
    }

}