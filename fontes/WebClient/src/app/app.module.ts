import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgModel } from '@angular/forms';
import { RouterModule } from '@angular/router'
import { appRoutes } from './app.route';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatButtonModule
       ,MatCheckboxModule
       ,MatToolbarModule
       ,MatMenuModule
       ,MatCardModule
       ,MatInputModule
       ,MatFormFieldModule
       ,MatOptionModule
       ,MatSelectModule
       ,MatTableModule} from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { MenuComponent } from './menu/menu.component';
import { InicioComponent } from './inicio/inicio.component';
import { QuestionFormComponent } from './question-form/question-form.component';
import { HttpModule } from '@angular/http';
import { TipFormComponent } from './tip-form/tip-form.component';
import { TipFindComponent } from './tip-find/tip-find.component';
import { QuestionFindComponent } from './question-find/question-find.component';
import { GameFormComponent } from './game-form/game-form.component';
import { GameFindComponent } from './game-find/game-find.component';
import { MatchFormComponent } from './match-form/match-form.component';
import { GameIslandComponent } from './game-island/game-island.component';
import { GameComponent } from './game/game.component';

import { QRCodeModule } from 'angular2-qrcode';
import { MatchFindComponent } from './match-find/match-find.component';
import { ReportsComponent } from './reports/reports.component';

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    InicioComponent,
    QuestionFormComponent,
    TipFormComponent,
    TipFindComponent,
    QuestionFindComponent,
    GameFormComponent,
    GameFindComponent,
    MatchFormComponent,
    GameIslandComponent,
    GameComponent,
    MatchFindComponent,
    ReportsComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatButtonModule, 
    MatCheckboxModule, 
    MatToolbarModule,
    MatMenuModule,
    MatCardModule,
    MatInputModule,
    MatFormFieldModule,
    FormsModule,
    HttpModule,
    MatOptionModule,
    MatSelectModule,
    ReactiveFormsModule,
    FormsModule,
    QRCodeModule,
    MatTableModule,
    RouterModule.forRoot(appRoutes, {useHash: true})
  ],
  providers: [],
  bootstrap: [AppComponent]
})

export class AppModule { }

platformBrowserDynamic().bootstrapModule(AppModule);
