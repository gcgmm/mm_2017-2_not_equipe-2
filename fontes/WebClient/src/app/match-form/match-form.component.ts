import { Component, OnInit } from '@angular/core';
import { Params, ActivatedRoute } from '@angular/router';

import {Match, matchVar} from '../../model/match-model'

import {MatchService} from '../../geocirc-services/match.service'
import {GameService} from '../../geocirc-services/game.service'

import { ViewDefault } from '../../common/view-default';
import { retry } from 'rxjs/operator/retry';

import {Router} from '@angular/router'
import { Game } from '../../model/game-model';

import * as moment from 'moment';

@Component({
  selector: 'app-match-form',
  templateUrl: './match-form.component.html',
  styleUrls: ['./match-form.component.css'],
  providers: [MatchService, GameService]
})
export class MatchFormComponent extends ViewDefault implements OnInit {
  
    public match : Match;
    public games: Array<Game>;
    public returnId;
  
    constructor(private router: Router, private route: ActivatedRoute, private matchService: MatchService, private gameService: GameService) { 
      super();
      this.match = new Match();
      this.limpar();
      this.games = new Array<Game>();
      this.getAllGames();
    }
  
    ngOnInit() {
    }
  
    cadastra(){
     // this.router.navigate(['game'] , { queryParams: {matchId: 0} });
      if (this.match.Descricao == ""){
        super.setErrorMessage("Informe a Descrição da Partida!");
      } else if (this.match.Jogo.Id == ""){
          super.setErrorMessage("Informe o Jogo!");
      } else {
        super.setSuccessMessage("Salvando a Partida...");
        var status = this.matchService.insertMatch(this.match);
        status.subscribe(
          response => {
            super.setSuccessMessage("Partida Salva com Sucesso!");
            this.limpar();
            matchVar.match = response;
            console.log("----------------");
            console.log(matchVar.match);

            this.router.navigate(['game']);
          },
          error => {
            super.setErrorMessageDefault();
          }
        );
        
      }
    }
  
    limpar(){
      this.match.Jogo = new Game();
      this.match.Descricao = "";
    }
  
    getAllGames(){
      super.setSuccessMessage("Carregando Jogos...");
      var status = this.gameService.getAllGames();
      status.subscribe(
        response => {
          this.games = response;
          super.setSuccessMessage("Jogos Carregados com Sucesso!");
        },
        error => {
          super.setErrorMessageDefault();
        }
      );
    }

}
  