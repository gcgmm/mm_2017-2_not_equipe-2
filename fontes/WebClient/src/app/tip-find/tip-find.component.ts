import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router'

import {TipService} from '../../geocirc-services/tip.service'

import {Tip} from '../../model/tip-model'
import { ViewDefault } from '../../common/view-default';

@Component({ 
  selector: 'app-tip-find',
  templateUrl: './tip-find.component.html',
  styleUrls: ['./tip-find.component.css'],
  providers: [TipService]
})
export class TipFindComponent extends ViewDefault implements OnInit {

  public tips: Array<Tip>;

  constructor(private router: Router, private tipService: TipService) {
    super();
    this.tips = new Array<Tip>();
    this.getAllTips();
  }

  ngOnInit() {}
  
  getAllTips(){
    super.setSuccessMessage("Carregando Dicas...");
    var status = this.tipService.getAllTips();
    status.subscribe(
      response => {
        this.tips = response;
        super.setSuccessMessage("Dicas Carregadas com Sucesso!");
      },
      error => {
        super.setErrorMessageDefault();
      }
    );
  }

  editar(tipParam: Tip) {
    this.router.navigate(['tipForm'], { queryParams: {tipId: tipParam.Id} });
  }

  excluir(tipParam : Tip) {
    super.setSuccessMessage("Excluindo Dica " + tipParam.Descricao  +"...");
    var status = this.tipService.deleteTip(tipParam.Id);
    status.subscribe(
      response => {
        super.setSuccessMessage("Dica " + tipParam.Descricao + " Excluída com Sucesso!");
        this.getAllTips();
      },
      error => {
        super.setErrorMessageDefault()
      }
    );
  }

}
