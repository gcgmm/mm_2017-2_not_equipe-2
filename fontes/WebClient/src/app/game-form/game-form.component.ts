import { Component, OnInit } from '@angular/core';
import { Params, ActivatedRoute } from '@angular/router';

import {Game} from '../../model/game-model'
import {Question} from '../../model/question-model'

import {GameService} from '../../geocirc-services/game.service'

import {QuestionService} from '../../geocirc-services/question.service'
import { ViewDefault } from '../../common/view-default';
import { retry } from 'rxjs/operator/retry';

import {Router} from '@angular/router'

@Component({
  selector: 'app-game-form',
  templateUrl: './game-form.component.html',
  styleUrls: ['./game-form.component.css'],
  providers: [GameService, QuestionService]
})
export class GameFormComponent extends ViewDefault implements OnInit {

  public game : Game;
  public newQuestion;

  public questions;

  constructor(private router: Router, private route: ActivatedRoute, private gameService: GameService, private questionService: QuestionService) { 
    super();
    this.game = new Game();
    this.limpar();
    this.route.queryParams.forEach(param => {
      if (param.gameId) {
        this.loadGame(param.gameId);
      }
    });
    this.getAllQuestions();
  }

  ngOnInit() {
  }

  cadastra(){
    if (this.game.Descricao == ""){
      super.setErrorMessage("Informe a Descrição do Jogo!");
    } else {
      super.setSuccessMessage("Salvando o Jogo...");
      var status;
      if (this.game.Id == 0){
        status = this.gameService.insertGame(this.game);
      } else {
        status = this.gameService.updateGame(this.game);
      }
      status.subscribe(
        response => {
          super.setSuccessMessage("Jogo Salvo com Sucesso!");
          this.limpar();
        },
        error => {
          super.setErrorMessageDefault();
        }
      );
    }
  }

  limpar(){
    this.game.Descricao = "";
  }

  add(){
    super.setSuccessMessage("Adicionando Questão...");
    var status = this.gameService.addQuestion(this.game.Id, this.newQuestion);
    status.subscribe(
      response => {
        super.setSuccessMessage("Questão Adicionada com Sucesso!");
        this.loadGame(this.game.Id);
      },
      error => {
        super.setErrorMessageDefault();
      }
    );
  }

  excluir(question: Question){
    super.setSuccessMessage("Excluindo Questão...");
    var status = this.gameService.removeQuestion(this.game.Id, question.Id);
    status.subscribe(
      response => {
        super.setSuccessMessage("Questão excluída com Sucesso!");
        this.loadGame(this.game.Id);
      },
      error => {
        super.setErrorMessageDefault();
      }
    );
  }

  editar(questionParam : Question) {
    this.router.navigate(['questionForm'], { queryParams: {questionId: questionParam.Id} });
  }

  loadGame(gameId){
    //super.setSuccessMessage("Carregando Jogo...");
    var status = this.gameService.loadGame(gameId);
    status.subscribe(
      response => {
        this.game = response;
        //super.setSuccessMessage("Jogo Carregado com Sucesso!");
      },
      error => {
        super.setErrorMessageDefault();
      }
    );
  }

  getAllQuestions(){
    super.setSuccessMessage("Carregando Questões...");
    var status = this.questionService.getAllQuestions();
    status.subscribe(
      response => {
          this.questions = response;
          super.setSuccessMessage("Questões Carregadas com Sucesso!");
        },
        error => {
          super.setErrorMessageDefault();
        }
    );
  }

}
