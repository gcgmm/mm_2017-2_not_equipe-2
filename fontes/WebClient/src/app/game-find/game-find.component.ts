import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router'

import {GameService} from '../../geocirc-services/game.service'

import {Game} from '../../model/game-model'
import { ViewDefault } from '../../common/view-default';

@Component({
  selector: 'app-game-find',
  templateUrl: './game-find.component.html',
  styleUrls: ['./game-find.component.css'],
  providers: [GameService]
})
export class GameFindComponent extends ViewDefault implements OnInit {
  
  games: Array<Game>;
  
  constructor(private router: Router, private gameService: GameService) {
    super();
    this.games = Array<Game>();
    this.getAllGames();
  }

  getAllGames(){
    super.setSuccessMessage("Carregando Jogos...");
    var status = this.gameService.getAllGames();
    status.subscribe(
      response => {
        this.games = response;
        super.setSuccessMessage("Jogos Carregados com Sucesso!");
      },
      error => {
        super.setErrorMessageDefault();
      }
    );
  }

  ngOnInit() {
  }

  editar(gameParam) {
    this.router.navigate(['gameForm'], { queryParams: {gameId: gameParam.Id} });
  }

  excluir(gameParam: Game) {
    super.setSuccessMessage("Excluindo Jogo " + gameParam.Descricao  +"...");
    var status = this.gameService.deleteGame(gameParam.Id);
    status.subscribe(
      response => {
        super.setSuccessMessage("Jogo " + gameParam.Descricao + " Excluído com Sucesso!");
        this.getAllGames();
      },
      error => {
        super.setErrorMessageDefault();
      }
    );
  }

}
