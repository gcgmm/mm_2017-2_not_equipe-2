import {ImageLayoutController} from './image-layout-controller'
import { Match } from '../../model/match-model';
import { Island } from '../../model/island-model';
import { GameIslandComponent } from './game-island.component';
import { IslandLog } from '../../model/island-log-model';
import { Game } from '../../model/game-model';
import { MatchRancking } from '../../model/racking-model';
import {MatTableDataSource} from '@angular/material';

import * as moment from 'moment';

export class IslandLayoutController {
    
  imageLayout1: ImageLayoutController;
  imageLayout2: ImageLayoutController;
  imageLayout3: ImageLayoutController;
  imageLayout4: ImageLayoutController;
  imageLayout5: ImageLayoutController;

  islandHeight;
  islandWidth;
  islandMarginTop;
  islandMarginLeft;
  islandRotate;

  qNumberValue = 2;
  qTextValue = "";
  qQr = '';
  lQr = '';
  qResText = "Arraste para cá!";

  logInHeightPC = 20;
  logInWidthPC = 87;
  logInMTopPC = 0;
  logInMLeftPC = 0;

  logInHeight;
  logInWidth;
  logInMTop;
  logInMLeft;

  ranckingTableHeightPC = 70;
  ranckingTableWidthPC = 96;
  ranckingTableMTopPC = 28;
  ranckingTableMLeftPC = 2;

  ranckingTableHeight;
  ranckingTableWidth;
  ranckingTableMTop;
  ranckingTableMLeft;

  qNumberHeightPC = 20;
  qNumberWidthPC = 13;
  qNumberMTopPC = 0;
  qNumberMLeftPC = 0;

  qNumberHeight;
  qNumberWidth;
  qNumberMTop;
  qNumberMLeft;

  qTextHeightPC = 20;
  qTextWidthPC = 74;
  qTextMTopPC = 0;
  qTextMLeftPC = 13;

  qTextHeight;
  qTextWidth;
  qTextMTop;
  qTextMLeft;

  qQrHeightPC = 20;
  qQrWidthPC = 13;
  qQrMTopPC = 0;
  qQrMLeftPC = 87;
  
  qQrHeight;
  qQrWidth;
  qQrMTop = 0;
  qQrMLeft = 0;
  qQrSize = 0;

  qResHeightPC = 34;
  qResWidthPC = 30;
  qResMTopPC = 63;
  qResMLeftPC = 35; 

  qResHeight;
  qResWidth;
  qResMTop;
  qResMLeft;
  lQrSize;

  qImgHeightPC = 35;
  qImgWidthPC = 31;

  response;

  image1NE;
  image2NE;
  image3NE;
  image4NE;
  image5NE;

  game: Game;

  active: boolean = false;
  hidden = "0";

  actualQuestion;

  homeMessage = "Bem Vindo(a) ao GEOCIRC!";
  instructionMessage = "Para começar o jogo use o aplicativo GEOCIRC para ler o QR Code.";

  gameMatch: Match;

  island: Island;
  islandNumber: number;
  gameIslandComponent: GameIslandComponent;

  dataInicial: string;

  images: Array<number> = [0,1,2,3,4];

  ranking: Array<MatchRancking> = new Array<MatchRancking>();

  displayedColumns = ['position', 'name', 'rightQuestions', 'wrongQuestions', 'tipQuestions'];
  matchRancking: MatTableDataSource<MatchRancking>;

  matchRanckingMTop;

  constructor(islandHeight: number, islandWidth: number, islandMarginTop: number, 
    islandMarginLeft: number, islandRotate: number, response, 
    image1NE, image2NE, image3NE, image4NE, image5NE, gameMatch: Match,
    islandNumber: number, gameIslandComponent: GameIslandComponent){

    this.islandHeight = islandHeight;
    this.islandWidth = islandWidth;
    this.islandMarginTop = islandMarginTop;
    this.islandMarginLeft = islandMarginLeft;
    this.islandRotate = islandRotate;
    this.image1NE = image1NE;
    this.image2NE = image2NE;
    this.image3NE = image3NE;
    this.image4NE = image4NE;
    this.image5NE = image5NE;
    this.gameMatch = gameMatch;
    this.islandNumber = islandNumber;
    this.gameIslandComponent = gameIslandComponent; 

    this.loadLogInValues();
    this.loadRanckingTableInValues();
    this.loadQNumberValues();
    this.loadQTextValues();
    this.loadQQrValues();
    
    this.matchRanckingMTop = this.getPXfromPCHeight(10);
    this.imageLayout1 = new ImageLayoutController(this, this.qImgHeightPC, this.qImgWidthPC, 25, 2,  this.image1NE, null, null);
    this.imageLayout2 = new ImageLayoutController(this, this.qImgHeightPC, this.qImgWidthPC, 25, 34, this.image2NE, null, null);
    this.imageLayout3 = new ImageLayoutController(this, this.qImgHeightPC, this.qImgWidthPC, 25, 67, this.image3NE, null, null);
    this.imageLayout4 = new ImageLayoutController(this, this.qImgHeightPC, this.qImgWidthPC, 63, 2,  this.image4NE, null, null);
    this.imageLayout5 = new ImageLayoutController(this, this.qImgHeightPC, this.qImgWidthPC, 63, 67, this.image5NE, null, null);
    this.loadResValues();
    
    this.response = response;
    this.lQr = '{"Type":"L","Id":"' + this.islandNumber + '","Match":"' + this.gameMatch.Id + '"}';
    this.instructionMessage = "Para começar o jogo " + this.gameMatch.Descricao + ", use o aplicativo GEOCIRC para ler o QR Code.";


    this.verifyIslandState(islandNumber);
    this.loadRanking(true);

  }

  loadRanking(resend: boolean){
    this.ranking = new Array<MatchRancking>();
    var status = this.gameIslandComponent.loadRancking(this.gameMatch.Id);
    status.subscribe(
      response => {
        this.ranking = response
        this.matchRancking = new MatTableDataSource<MatchRancking>(this.ranking);
        if (resend){
          setTimeout(() => this.loadRanking(resend), 30000);
        }
      },
      error => {
        if (resend){
          setTimeout(() => this.loadRanking(resend), 30000);
        }
      }
    );
    
    /*var matchRancking: MatchRancking = new MatchRancking();
    matchRancking.Nome = "Xandra";
    matchRancking.Pontos = 176;
    matchRancking.Corretas = 60;
    matchRancking.Erradas = 10;
    matchRancking.Dicas = 9;
    this.ranking.push(matchRancking);

    matchRancking = new MatchRancking();
    matchRancking.Nome = "Roberto";
    matchRancking.Pontos = 50;
    matchRancking.Corretas = 10;
    matchRancking.Erradas = 3;
    matchRancking.Dicas = 5;
    this.ranking.push(matchRancking);

    matchRancking = new MatchRancking();
    matchRancking.Nome = "Alfredo";
    matchRancking.Pontos = 40;
    matchRancking.Corretas = 10;
    matchRancking.Erradas = 10;
    matchRancking.Dicas = 10;
    this.ranking.push(matchRancking);

    matchRancking = new MatchRancking();
    matchRancking.Nome = "Bily";
    matchRancking.Pontos = 39;
    matchRancking.Corretas = 10;
    matchRancking.Erradas = 11;
    matchRancking.Dicas = 10;
    this.ranking.push(matchRancking);

    matchRancking = new MatchRancking();
    matchRancking.Nome = "Gumercinda";
    matchRancking.Pontos = 30;
    matchRancking.Corretas = 10;
    matchRancking.Erradas = 15;
    matchRancking.Dicas = 33;
    this.ranking.push(matchRancking);

    matchRancking = new MatchRancking();
    matchRancking.Nome = "Astolfo";
    matchRancking.Pontos = 20;
    matchRancking.Corretas = 5;
    matchRancking.Erradas = 10;
    matchRancking.Dicas = 6;
    this.ranking.push(matchRancking);*/
    
  }

  verifyIslandState(islandNumber: number){
    var status = this.gameIslandComponent.configMatchServiceGetIsland(islandNumber);
    status.subscribe(
      response => {
        this.island = response
        if (this.island == null || this.island.Aluno == null){
          setTimeout(() => this.verifyIslandState(islandNumber), 3000);
        } else {
          this.jogar();
        }
      },
      error => {
        setTimeout(() => this.verifyIslandState(islandNumber), 3000);
      }
    );
  }

  configureImages(i:number){
    this.shuffleImages();
    this.qNumberValue = i + 1;

    this.qTextValue =  this.gameMatch.Jogo.Perguntas[i].Descricao;
    this.qQr = '{"Type":"D","Id":"' + 
                this.gameMatch.Jogo.Perguntas[i].IdDica + 
                '","Match":"' + this.gameMatch.Id + '"}'

    var image;
    var imageType;

    for (var j = 0; j < this.images.length; j++) {
      if (this.images[j] == 0){
        image = this.gameMatch.Jogo.Perguntas[i].ImagemCerta;
        imageType = this.gameMatch.Jogo.Perguntas[i].TipoImagemCerta;
      } else if (this.images[j] == 1){
        image = this.gameMatch.Jogo.Perguntas[i].Imagem1;
        imageType = this.gameMatch.Jogo.Perguntas[i].TipoImagem1;
      } else if (this.images[j] == 2){
        image = this.gameMatch.Jogo.Perguntas[i].Imagem2;
        imageType = this.gameMatch.Jogo.Perguntas[i].TipoImagem2;
      } else if (this.images[j] == 3){
        image = this.gameMatch.Jogo.Perguntas[i].Imagem3;
        imageType = this.gameMatch.Jogo.Perguntas[i].TipoImagem3;
      } else if (this.images[j] == 4){
        image = this.gameMatch.Jogo.Perguntas[i].Imagem4;
        imageType = this.gameMatch.Jogo.Perguntas[i].TipoImagem4;
      }

      if (j == 0){
        this.imageLayout1.image = image;
        this.imageLayout1.imageType = imageType;
      } else if (j == 1){
        this.imageLayout2.image = image;
        this.imageLayout2.imageType = imageType;
      } else if (j == 2){
        this.imageLayout3.image = image;
        this.imageLayout3.imageType = imageType;
      } else if (j == 3){
        this.imageLayout4.image = image;
        this.imageLayout4.imageType = imageType;
      } else if (j == 4){
        this.imageLayout5.image = image;
        this.imageLayout5.imageType = imageType;
      }

    }

    /*
//    this.imageLayout1.image = "";
    this.imageLayout1.image = this.gameMatch.Jogo.Perguntas[i].ImagemCerta;
    this.imageLayout1.imageType = this.gameMatch.Jogo.Perguntas[i].TipoImagemCerta;
 //this.imageLayout1.loadValues();
 //this.imageLayout2.image = "";
    this.imageLayout2.image = this.gameMatch.Jogo.Perguntas[i].Imagem1;
    this.imageLayout2.imageType = this.gameMatch.Jogo.Perguntas[i].TipoImagem1;
    //this.imageLayout2.loadValues();
    //this.imageLayout3.image = "";
    this.imageLayout3.image =  this.gameMatch.Jogo.Perguntas[i].Imagem2;
    this.imageLayout3.imageType = this.gameMatch.Jogo.Perguntas[i].TipoImagem2;
    //this.imageLayout3.loadValues();
    //this.imageLayout4.image = "";
    this.imageLayout4.image = this.gameMatch.Jogo.Perguntas[i].Imagem3;
    this.imageLayout4.imageType = this.gameMatch.Jogo.Perguntas[i].TipoImagem3;
    //this.imageLayout4.loadValues();
    //this.imageLayout5.image = "";
    this.imageLayout5.image =  this.gameMatch.Jogo.Perguntas[i].Imagem4;
    this.imageLayout5.imageType = this.gameMatch.Jogo.Perguntas[i].TipoImagem4;
    //this.imageLayout5.loadValues();
*/
    this.dataInicial = moment().format("YYYY/MM/DD HH:mm:ss");
    
  }
  
  loadRanckingTableInValues(){
    this.ranckingTableHeight = this.getPXfromPCHeight(this.ranckingTableHeightPC);
    this.ranckingTableWidth  = this.getPXfromPCWidth(this.ranckingTableWidthPC);
    this.ranckingTableMTop   = this.getPXfromPCHeight(this.ranckingTableMTopPC);
    this.ranckingTableMLeft  = this.getPXfromPCWidth(this.ranckingTableMLeftPC);
  }

  loadLogInValues(){
    this.logInHeight = this.getPXfromPCHeight(this.logInHeightPC);
    this.logInWidth  = this.getPXfromPCWidth(this.logInWidthPC);
    this.logInMTop   = this.getPXfromPCHeight(this.logInMTopPC);
    this.logInMLeft  = this.getPXfromPCWidth(this.logInMLeftPC);
  }

  loadQNumberValues(){
    this.qNumberHeight = this.getPXfromPCHeight(this.qNumberHeightPC);
    this.qNumberWidth  = this.getPXfromPCWidth(this.qNumberWidthPC);
    this.qNumberMTop   = this.getPXfromPCHeight(this.qNumberMTopPC);
    this.qNumberMLeft  = this.getPXfromPCWidth(this.qNumberMLeftPC);
  }

  loadQTextValues(){
    this.qTextHeight = this.getPXfromPCHeight(this.qTextHeightPC);
    this.qTextWidth  = this.getPXfromPCWidth(this.qTextWidthPC);
    this.qTextMTop   = this.getPXfromPCHeight(this.qTextMTopPC);
    this.qTextMLeft  = this.getPXfromPCWidth(this.qTextMLeftPC);
  }

  loadQQrValues(){
    this.qQrHeight = this.getPXfromPCHeight(this.qQrHeightPC);
    this.qQrWidth  = this.getPXfromPCWidth(this.qQrWidthPC);
    this.qQrMTop   = this.getPXfromPCHeight(this.qQrMTopPC);
    this.qQrMLeft  = this.getPXfromPCWidth(this.qQrMLeftPC);
    this.qQrSize = this.qQrHeight >  this.qQrWidth ? this.qQrWidth : this.qQrHeight

  }

  loadResValues(){
    this.qResHeight = this.getPXfromPCHeight(this.qResHeightPC);
    this.qResWidth  = this.getPXfromPCWidth(this.qResWidthPC);
    this.qResMTop   = this.getPXfromPCHeight(this.qResMTopPC);
    this.qResMLeft  = this.getPXfromPCWidth(this.qResMLeftPC);
    this.lQrSize = this.qResHeight >  this.qResWidth ? this.qResWidth : this.qResHeight
  }

  getPXfromPC(px, pc){
      return (px * pc) / 100;
  }

  getPXfromPCHeight(pc){
    return this.getPXfromPC(this.islandHeight, pc);
  }
  
  getPXfromPCWidth(pc){
    return this.getPXfromPC(this.islandWidth, pc);
  }

  
  getRotateText() : String{
    return "rotate(" + this.islandRotate + "deg)";
  }

  isImageInTarget(qMLeftMov: number, qMTopMov: number, imageNumber: number){
    var inside: boolean = qMLeftMov > this.qResMLeft &&
                          qMLeftMov < this.qResMLeft + this.qResWidth &&
                          qMTopMov > this.qResMTop &&
                          qMTopMov < this.qResMTop + this.qResHeight;

    if (inside){ 
      if (this.images[imageNumber - 1] == 0){
        if (this.actualQuestion >= this.gameMatch.Jogo.Perguntas.length - 1){
          this.active = false;
          this.hidden = "0";
          this.homeMessage = "Parabéns " + this.island.Aluno.NomeGoogle + "! Você acertou todas as questões."
          this.sendGameLog(this.images[imageNumber - 1], true);
        } else {
          this.sendGameLog(this.images[imageNumber - 1], false);
          this.actualQuestion++;
          this.configureImages(this.actualQuestion);
        }
      } else {
        this.active = false;
        this.hidden = "0";
        this.homeMessage = "Ah " + this.island.Aluno.NomeGoogle + ", você errou! Tente novamente após consultar a dica."
        this.sendGameLog(this.images[imageNumber - 1], true);
      }
    }

    return inside;
/*
    console.log("response: " + this.response.nativeElement.offsetWidth);
    console.log("response: " + this.response.nativeElement.offsetHeight);
    
    console.log("image1NE: " + this.image1NE.nativeElement.offsetWidth);
    console.log("image1NE: " + this.image1NE.nativeElement.offsetHeight);*/

    /*console.log("getPXfromPCHeight: " + this.response.nativeElement.getPXfromPCHeight);
    console.log("getPXfromPCWidth: " + this.response.nativeElement.getPXfromPCWidth);

    console.log(this.response.nativeElement.style);

    console.log(this.response.nativeElement.style.marginLeft);
    console.log(this.response.nativeElement.style.marginTop);*/

//    console.log("---------------------------------");

  }

  sendGameLog(imageNumber: number, reset: boolean){
    var islandLog: IslandLog = new IslandLog(); 
    islandLog.Aluno = this.island.Aluno;
    islandLog.IdPartida = this.gameMatch.Id;
    islandLog.IdPergunta = this.gameMatch.Jogo.Perguntas[this.actualQuestion].Id;   
    islandLog.DataHoraInicio = this.dataInicial;
    islandLog.DataHoraFim = moment().format("YYYY/MM/DD HH:mm:ss");
    islandLog.SequenciaResposta = imageNumber;
    islandLog.SequenciaIlha = this.islandNumber;
    var status = this.gameIslandComponent.configMatchServiceSendLogData(islandLog);
    status.subscribe(
      response => {
        console.log("foi!");
        if (reset){
          this.verifyIslandState(this.islandNumber);
          this.loadRanking(false);
        }
      },
      error => {
        console.log("ná");
        if (reset){
          this.verifyIslandState(this.islandNumber);
          this.loadRanking(false);
        }
      }
    );
  }

  jogar(){
    this.active = true;
    this.hidden = "100";
    this.actualQuestion = 0;
    this.configureImages(0);
  }

  getHiddenState(){
    if (this.active){
      return "100";
    }
    return "0";
  }

  getShowState(){
    if (!this.active){
      return "100";
    }
    return "0";
  }

  getHiddenIState(){
    if (this.active){
      return "100";
    }
    return "0";
  }

  getShowIState(){
    if (!this.active){
      return "100";
    }
    return "0";
  }

  shuffleImages() {
    var j, x, i;
    for (i = this.images.length - 1; i > 0; i--) {
        j = Math.floor(Math.random() * (i + 1));
        x = this.images[i];
        this.images[i] = this.images[j];
        this.images[j] = x;
    }
  }


}