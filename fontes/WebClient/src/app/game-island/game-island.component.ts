import { Component, OnInit, ViewChild } from '@angular/core';

import {IslandLayoutController} from './island-layout-controller'
import { Match, matchVar } from '../../model/match-model';
import { Question } from '../../model/question-model';

import {MatchService} from '../../geocirc-services/match.service'

import {ActivatedRoute, Router} from '@angular/router'
import { IslandLog } from '../../model/island-log-model';

import {QRCodeComponent} from 'angular2-qrcode';

import * as moment from 'moment';

@Component({
  selector: 'app-game-island',
  templateUrl: './game-island.component.html',
  styleUrls: ['./game-island.component.css'],
  providers: [MatchService, QRCodeComponent]
})
export class GameIslandComponent implements OnInit {
 
  @ViewChild('root') root;  
  @ViewChild('response') response;  
  
  @ViewChild('image1NE')  image1NE;
  @ViewChild('image2NE')  image2NE;
  @ViewChild('image3NE')  image3NE;
  @ViewChild('image4NE')  image4NE;
  @ViewChild('image5NE')  image5NE;

  @ViewChild('image1NE90')  image1NE90;
  @ViewChild('image2NE90')  image2NE90;
  @ViewChild('image3NE90')  image3NE90;
  @ViewChild('image4NE90')  image4NE90;
  @ViewChild('image5NE90')  image5NE90;

  @ViewChild('image1NE180')  image1NE180;
  @ViewChild('image2NE180')  image2NE180;
  @ViewChild('image3NE180')  image3NE180;
  @ViewChild('image4NE180')  image4NE180;
  @ViewChild('image5NE180')  image5NE180;

  @ViewChild('image1NE270')  image1NE270;
  @ViewChild('image2NE270')  image2NE270;
  @ViewChild('image3NE270')  image3NE270;
  @ViewChild('image4NE270')  image4NE270;
  @ViewChild('image5NE270')  image5NE270;


  islandHeight;
  islandWidth;
  rootHeight;
  rootWidth;

  island1: IslandLayoutController;
  island2: IslandLayoutController;
  island3: IslandLayoutController;
  island4: IslandLayoutController;
  
  gameMatch;
  
  constructor(private router: Router, 
    private route: ActivatedRoute, 
    private matchService: MatchService) {}

  ngOnInit() {
    this.loadMatchData();
  }

  loadMatchData(){
    this.gameMatch = matchVar.match;
    console.log(this.gameMatch.Id);
    if (this.gameMatch.Id != null){
      this.start();
      this.verifyEndGame();
    } else {
      this.router.navigate(['matchForm']);
    }
  }

  verifyEndGame(){
    var status = this.matchService.getMatch(this.gameMatch.Id);
    status.subscribe(
      response => {

        var match: Match = response;
        if (match.DataHoraFim == ''){
          setTimeout(() => this.verifyEndGame(), 15000);
        } else {
          this.router.navigate(['reports'], { queryParams: {matchId: this.gameMatch.Id} });
        }
      },
      error => {
        setTimeout(() => this.verifyEndGame(), 15000);
      }
    );
  }



  start(){
    this.rootHeight = this.root.nativeElement.offsetHeight;
    this.rootWidth = this.root.nativeElement.offsetWidth;

    this.islandHeight = this.rootHeight / 2.2;
    this.islandWidth = this.rootWidth / 2.2;
    
    this.island1 = new IslandLayoutController(this.islandHeight, this.islandWidth, 
      this.rootHeight - this.islandHeight , (this.rootWidth / 2) - (this.islandWidth/2), 0, this.response, 
      this.image1NE, this.image2NE, this.image3NE, this.image4NE, this.image5NE, this.gameMatch, 1, this);

    this.island2 = new IslandLayoutController(this.islandHeight, this.islandWidth, 
    - ((this.islandHeight/2) - (this.islandWidth/2)) + (this.rootHeight / 2) - (this.islandWidth / 2), - 
    ((this.islandWidth/2) - (this.islandHeight/2)) , 90, this.response,
      this.image1NE90, this.image2NE90, this.image3NE90, this.image4NE90, this.image5NE90, this.gameMatch, 2, this);

    this.island3 = new IslandLayoutController(this.islandHeight, this.islandWidth,
       0, (this.rootWidth / 2) - (this.islandWidth/2), 180, this.response,
       this.image1NE180, this.image2NE180, this.image3NE180, this.image4NE180, this.image5NE180, this.gameMatch, 3, this);

    this.island4 = new IslandLayoutController(this.islandHeight, this.islandWidth, 
    - ((this.islandHeight/2) - (this.islandWidth/2)) + (this.rootHeight / 2) - (this.islandWidth / 2), this.rootWidth - 
    this.islandHeight - ((this.islandWidth/2) - (this.islandHeight/2)), 270, this.response,
    this.image1NE270, this.image2NE270, this.image3NE270, this.image4NE270, this.image5NE270, this.gameMatch, 4, this);
    
  }

  getImageBase64(imageType: string, image: string){
    if (imageType == null || image == null){
      return null;
    }
    return 'data:' + imageType + ';base64,' + image;
  }

  configMatchServiceGetIsland(islandNumber: number){
    return this.matchService.getIsland(islandNumber);
  }

  configMatchServiceSendLogData(islandLog: IslandLog){
    return this.matchService.sendLog(islandLog);
  }

  loadRancking(id: number){
    return this.matchService.getMatchRancking(id);
  }

}