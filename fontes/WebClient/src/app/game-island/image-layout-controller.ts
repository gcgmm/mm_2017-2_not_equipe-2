
import {IslandLayoutController} from './island-layout-controller'

export class ImageLayoutController {

    islandLayoutController: IslandLayoutController;

    qHeightPC;
    qWidthPC;
    qMTopPC;
    qMLeftPC;

    qHeight;
    qWidth;
    qMTop;
    qMLeft;
    qMTopMov;
    qMLeftMov;

    qMLeftMovTemp;
    qMTopMovTemp;

    imageNE;
    image;
    imageType;
    
    imageWidth;
    imageHeight;

    imageAjust: boolean = false; 

    zIndex;
    defaultZIndex = 100;    
    panZIndex = 500;

    constructor(islandLayoutController: IslandLayoutController, qHeightPC: number, 
        qWidthPC: number, qMTopPC: number, qMLeftPC: number, imageNE, image, imageType){
        this.islandLayoutController = islandLayoutController;
        this.qHeightPC = qHeightPC;
        this.qWidthPC = qWidthPC;
        this.qMTopPC = qMTopPC;
        this.qMLeftPC = qMLeftPC;
        this.imageNE = imageNE;
        this.image = image;
        this.imageType = imageType;
        this.zIndex = this.defaultZIndex;
        this.loadValues();
    }

    loadValues(){
        this.qHeight = this.islandLayoutController.getPXfromPCHeight(this.qHeightPC);
        this.qWidth  = this.islandLayoutController.getPXfromPCWidth(this.qWidthPC);
        this.qMTop   = this.islandLayoutController.getPXfromPCHeight(this.qMTopPC);
        this.qMLeft  = this.islandLayoutController.getPXfromPCWidth(this.qMLeftPC);
        this.qMTopMov   = this.qMTop;
        this.qMLeftMov  = this.qMLeft;
        
    }

    onPanStart(event: any): void {
      event.preventDefault();
      this.qMLeftMovTemp = this.qMLeftMov;
      this.qMTopMovTemp = this.qMTopMov;
      this.zIndex = this.panZIndex;
    }
  
    onPan(event: any): void {
      event.preventDefault();
      this.qMLeftMov = this.qMLeftMovTemp + event.deltaX;
      this.qMTopMov = this.qMTopMovTemp + event.deltaY;
    }
  
    onPanEnd(event: any, imageId: number): void {
      this.islandLayoutController.isImageInTarget(this.qMLeftMov + (this.imageWidth/2), 
          this.qMTopMov + (this.imageHeight/2), imageId);
      this.qMLeftMov = this.qMLeft;
      this.qMTopMov = this.qMTop;    
      this.zIndex = this.defaultZIndex;
    }

    onPanStart90(event: any): void {
      event.preventDefault();
      this.qMLeftMovTemp = this.qMLeftMov;
      this.qMTopMovTemp = this.qMTopMov;
      this.zIndex = this.panZIndex;
    }
  
    onPan90(event: any): void {
      event.preventDefault();
      this.qMLeftMov = this.qMLeftMovTemp + event.deltaY;
      this.qMTopMov = this.qMTopMovTemp - event.deltaX;
    }
  
    onPanEnd90(event: any, imageId: number): void {
      this.islandLayoutController.isImageInTarget(this.qMLeftMov + (this.imageWidth/2), 
      this.qMTopMov + (this.imageHeight/2), imageId);
      this.qMLeftMov = this.qMLeft;
      this.qMTopMov = this.qMTop; 
      this.zIndex = this.defaultZIndex;   
}

    onPanStart180(event: any): void {
      event.preventDefault();
      this.qMLeftMovTemp = this.qMLeftMov;
      this.qMTopMovTemp = this.qMTopMov;
      this.zIndex = this.panZIndex;
    }
  
    onPan180(event: any): void {
      event.preventDefault();
      this.qMLeftMov = this.qMLeftMovTemp - event.deltaX;
      this.qMTopMov = this.qMTopMovTemp - event.deltaY;
    }
  
    onPanEnd180(event: any, imageId: number): void {
      this.islandLayoutController.isImageInTarget(this.qMLeftMov + (this.imageWidth/2), 
      this.qMTopMov + (this.imageHeight/2), imageId);
      this.qMLeftMov = this.qMLeft;
      this.qMTopMov = this.qMTop; 
      this.zIndex = this.defaultZIndex;   
    }

    onPanStart270(event: any): void {
      event.preventDefault();
      this.qMLeftMovTemp = this.qMLeftMov;
      this.qMTopMovTemp = this.qMTopMov;
      this.zIndex = this.panZIndex;
    }
  
    onPan270(event: any): void {
      event.preventDefault();
      this.qMLeftMov = this.qMLeftMovTemp - event.deltaY ;
      this.qMTopMov = this.qMTopMovTemp + event.deltaX;
    }
  
    onPanEnd270(event: any, imageId: number): void {
      this.islandLayoutController.isImageInTarget(this.qMLeftMov + (this.imageWidth/2), 
      this.qMTopMov + (this.imageHeight/2), imageId);
      this.qMLeftMov = this.qMLeft;
      this.qMTopMov = this.qMTop;   
      this.zIndex = this.defaultZIndex; 
    }

    getImageBase64(imageType: string, image: string){
      return 'data:' + imageType + ';base64,' + image;
    }
    
    async getLeftMarginValue(){
      /*console.log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
      
      console.log("this.imageHeight " + this.imageHeight);
      console.log("this.imageWidth " + this.imageWidth);
      console.log("this.imageHeight " + this.imageNE.nativeElement.offsetHeight);
      console.log("this.imageWidth " + this.imageNE.nativeElement.offsetWidth);*/
      if (!this.imageAjust){
        this.imageHeight = this.imageNE.nativeElement.offsetHeight; 
        this.imageWidth = this.imageNE.nativeElement.offsetWidth;
        
        if (this.imageWidth > 0  && this.imageWidth < this.qWidth){
      /*    console.log("entrou");
          console.log("this.qWidth " + this.qWidth);
          console.log("this.imageWidth " + this.imageWidth);*/
          
     //     console.log(this.qMLeft);
          this.qMLeft += ((this.qWidth - this.imageWidth) / 2);
      //    console.log(this.qMLeft);
          this.qMLeftMov = this.qMLeft;
          this.imageAjust = true;
        }
      }

      /*console.log("this.imageHeight " + this.imageHeight);
      console.log("this.imageWidth " + this.imageWidth);

      console.log("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");*/
      return this.qMLeftMov;
    } 


    adjustImageLayout(){
      //console.log("adjust");
      this.loadValues();
      this.imageHeight = this.imageNE.nativeElement.offsetHeight; 
      this.imageWidth = this.imageNE.nativeElement.offsetWidth;
      if (this.imageWidth > 0  && this.imageWidth < this.qWidth){
            this.qMLeft += ((this.qWidth - this.imageWidth) / 2);
            this.qMLeftMov = this.qMLeft;
            this.imageAjust = true;
      }
      
    }







}