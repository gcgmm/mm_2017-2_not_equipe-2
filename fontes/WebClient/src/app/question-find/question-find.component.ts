import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router'

import {Question} from '../../model/question-model'
import {QuestionService} from '../../geocirc-services/question.service'
import { ViewDefault } from '../../common/view-default';

@Component({
  selector: 'app-question-find',
  providers:[QuestionService],
  templateUrl: './question-find.component.html',
  styleUrls: ['./question-find.component.css']
})

export class QuestionFindComponent extends ViewDefault implements OnInit {

  public questions: Array<Question>;
  
  constructor(private router: Router,  private questionService: QuestionService) { 
    super();
    this.questions = new Array();
    this.getAllQuestions();
  }
  
  ngOnInit() {}

  getAllQuestions(){
    super.setSuccessMessage("Carregando Questões...");
    var status = this.questionService.getAllQuestions();
    status.subscribe(
      response => {
          this.questions = response;
          super.setSuccessMessage("Questões Carregadas com Sucesso!");
        },
        error => {
          super.setErrorMessageDefault();
        }
    );
  }

  editar(questionParam : Question) {
    this.router.navigate(['questionForm'], { queryParams: {questionId: questionParam.Id} });
  }

  excluir(questionParam : Question) {
    super.setSuccessMessage("Excluindo Questão " + questionParam.Descricao  +"...");
    var status = this.questionService.deleteQuestion(questionParam.Id);
    status.subscribe(
      response => {
        super.setSuccessMessage("Questão " + questionParam.Descricao + " Excluída com Sucesso!");
        this.getAllQuestions();
      },
      error => {
        super.setErrorMessageDefault();
      }
    );

  }

}
