import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router'

@Component({
    selector: 'app-menu',
    templateUrl: './menu.component.html',
    styleUrls: ['./menu.component.css'],
    inputs: ['formCadastro', 'navi', 'formAtual']
})
export class MenuComponent implements OnInit {

    @Input('formCadastro') formCadastro;
    @Input('navi') navi;
    @Input('formAtual') formAtual;        
    @Input('component') component;  

    public userId;

    constructor(private router: Router) {
    }

    ngOnInit() {   
        localStorage.setItem('userId', 'Roberto Weege Jr');
        this.userId = localStorage.getItem('userId');                    
    }

    novoRegistro() {  
        if (this.formCadastro != undefined && this.formCadastro != "") {
            this.router.navigate([this.formCadastro], {queryParams: {novoRegistro: true}});
        } else if (this.formAtual != undefined && this.formAtual != "") {
            this.router.navigate([this.formAtual], {queryParams: {novoRegistro: true}});                         
            this.component.novoRegistro();
        }        
    }

    sair() {
        //localStorage.removeItem('id_token');
        //this.router.navigate(['login']);
        this.router.navigate(['menu']);
    }

    openQuestionForm() {
        this.router.navigate(['questionForm']);
    }
    openQuestionFind() {
        this.router.navigate(['questionFind']);
    }
    openTipForm() {
        this.router.navigate(['tipForm']);
    }
    openTipFind() {
        this.router.navigate(['tipFind']);
    }
    openGameForm(){
        this.router.navigate(['gameForm']);
    }
    openGameFind(){
        this.router.navigate(['gameFind']);
    }
    openMatchForm(){
        this.router.navigate(['matchForm']);
    }
    openMatchFind(){
        this.router.navigate(['matchFind']);
    }
    openReports(){
        this.router.navigate(['reports']);
    }
}
