import { Component, OnInit } from '@angular/core';
import { MatchRancking } from '../../model/racking-model';
import { MatTableDataSource } from '@angular/material';
import { MatchService } from '../../geocirc-services/match.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.css'],
  providers: [MatchService]
})
export class ReportsComponent implements OnInit {

  constructor(private matchService: MatchService, private router: Router, private route: ActivatedRoute) { 
    this.route.queryParams.forEach(param => {
      if (param.matchId) {
        this.loadRanking(param.matchId);       
      };
    });
  }
  
  ranking: Array<MatchRancking> = new Array<MatchRancking>();
  matchRancking: MatTableDataSource<MatchRancking>;

  displayedColumns = ['position', 'name', 'rightQuestions', 'wrongQuestions', 'tipQuestions'];

  ngOnInit() {
  }

  loadRanking(id: number){
    this.ranking = new Array<MatchRancking>();
    var status = this.matchService.getMatchRancking(id);
    status.subscribe(
      response => {
        this.ranking = response
        this.matchRancking = new MatTableDataSource<MatchRancking>(this.ranking);
      },
      error => {
      }
    );
  }


}
