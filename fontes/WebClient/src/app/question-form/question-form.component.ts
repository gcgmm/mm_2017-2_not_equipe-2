import { Component, OnInit } from '@angular/core';
import { Params, ActivatedRoute } from '@angular/router';
import { NgModel } from '@angular/forms';
import { ViewChild } from '@angular/core';

import {Question} from '../../model/question-model'
import {Tip} from '../../model/tip-model'

import {QuestionService} from '../../geocirc-services/question.service'
import {TipService} from '../../geocirc-services/tip.service'
import { ViewDefault } from '../../common/view-default';

@Component({
  selector: 'app-question-form',
  providers:[QuestionService, TipService],
  templateUrl: './question-form.component.html',
  styleUrls: ['./question-form.component.css']
})
export class QuestionFormComponent extends ViewDefault implements OnInit {

  question: Question;  
  tips: Array<Tip>;

  constructor(private route: ActivatedRoute,  private questionService: QuestionService, private tipService: TipService) {
    super();
    this.question = new Question();
    this.limpar();
    this.loadTips();
    this.route.queryParams.forEach(param => {
      if (param.questionId) {
        this.loadQuestion(param.questionId);
      }
    });
    this.getAllTips();
  }

  ngOnInit() {
  }

  limpar(){
    this.question.Descricao = "";
    this.question.ImagemCerta = "";
    this.question.TipoImagemCerta = "";
    this.question.Imagem1 = "";
    this.question.TipoImagem1 = "";
    this.question.Imagem2 = "";
    this.question.TipoImagem2 = "";
    this.question.Imagem3 = "";
    this.question.TipoImagem3 = "";
    this.question.Imagem4 = "";
    this.question.TipoImagem4 = "";
    this.question.IdDica = "";
  }

  private getReaderFromFile(event) {
    if(event.target.files && event.target.files.length > 0) {
      let reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]);
      return reader;    
    } 
    return null;
  }

  validateImageFormat(file){
    if (file.type.split('/')[0] != 'image'){
      super.setErrorMessage("O Arquivo Deve ser uma Imagem!");
      return false;
    }
    return true;
  } 

  onFileChangeRR(event) {   
    let reader = this.getReaderFromFile(event);
    if(reader) {
      reader.onload = () => {
        if (this.validateImageFormat(event.target.files[0])){
          this.question.TipoImagemCerta = event.target.files[0].type;
          this.question.ImagemCerta = reader.result.split(',')[1];
        }
      };    
    }
  }
  onFileChangeRW1(event) {   
    let reader = this.getReaderFromFile(event);
    if(reader) {
      reader.onload = () => {
        if (this.validateImageFormat(event.target.files[0])){
          this.question.TipoImagem1 = event.target.files[0].type;
          this.question.Imagem1 = reader.result.split(',')[1];
        }
      };    
    }
  }

  onFileChangeRW2(event) {   
    let reader = this.getReaderFromFile(event);
    if(reader) {
      reader.onload = () => {
        if (this.validateImageFormat(event.target.files[0])){
          this.question.TipoImagem2 = event.target.files[0].type;
          this.question.Imagem2 = reader.result.split(',')[1];
        }
      };    
    }
  }  

  onFileChangeRW3(event) {   
    let reader = this.getReaderFromFile(event);
    if(reader) {
      reader.onload = () => {
        if (this.validateImageFormat(event.target.files[0])){
          this.question.TipoImagem3 = event.target.files[0].type;
          this.question.Imagem3 = reader.result.split(',')[1];
        }
      };    
    }
  }
  
  onFileChangeRW4(event) {   
    let reader = this.getReaderFromFile(event);
    if(reader) {
      reader.onload = () => {
        if (this.validateImageFormat(event.target.files[0])){
          this.question.TipoImagem4 = event.target.files[0].type;
          this.question.Imagem4 = reader.result.split(',')[1];
        }
      };    
    }
  }



  cadastra(){
    if (this.question.Descricao == ""){
      super.setErrorMessage("Informe a Pergunta!");
    } else if (this.question.ImagemCerta == "") {
      super.setErrorMessage("Selecione uma Imagem como Imagem Correta!");
    } else if (this.question.TipoImagemCerta == "") {
      super.setErrorMessage("O tipo da Imagem Correta não foi carregado.");
    } else if (this.question.Imagem1 == "") {
      super.setErrorMessage("Selecione uma Imagem como Imagem Errada 1!");
    } else if (this.question.TipoImagem1 == "") {
      super.setErrorMessage("O tipo da Imagem 1 não foi carregado.");
    } else if (this.question.Imagem2 == "") {
      super.setErrorMessage("Selecione uma Imagem como Imagem Errada 2!");
    } else if (this.question.TipoImagem2 == "") {
      super.setErrorMessage("O tipo da Imagem 2 não foi carregado.");
     } else if (this.question.Imagem3 == "") {
      super.setErrorMessage("Selecione uma Imagem como Imagem Errada 3!");
    } else if (this.question.TipoImagem3 == "") {
      super.setErrorMessage("O tipo da Imagem 3 não foi carregado.");
    } else if (this.question.Imagem4 == "") {
      super.setErrorMessage("Selecione uma Imagem como Imagem Errada 4!");
    } else if (this.question.TipoImagem4 == "") {
      super.setErrorMessage("O tipo da Imagem 4 não foi carregado.");
    } else if (this.question.IdDica == "") {
      super.setErrorMessage("Selecione uma Dica!");
    } else {
      super.setSuccessMessage("Salvando Questão...");
      var status;
      if (this.question.Id == 0){
        status = this.questionService.insertQuestion(this.question);
      } else {
        status = this.questionService.updateQuestion(this.question);
      }
      status.subscribe(
        response => {
          super.setSuccessMessage("Questão Salva com Sucesso!");
        },
        error => {
          super.setErrorMessageDefault();
        }
      );
    }
  }

  loadQuestion(tipId){
    super.setSuccessMessage("Carregando Questão...");
    var status = this.questionService.loadQuestion(tipId);
    status.subscribe(
      response => {
        this.question = response;
        super.setSuccessMessage("Questão Carregada com Sucesso");
      },
      error => {
        super.setErrorMessageDefault();
      }
    );
  }
  
  loadTips(){
    this.tips = new Array();
  }

  getAllTips(){
    super.setSuccessMessage("Carregando Dicas...");
    var status = this.tipService.getAllTips();
    status.subscribe(
      response => {
        this.tips = response;
        super.setSuccessMessage("Dicas Carregadas com Sucesso!");
      },
      error => {
        super.setErrorMessageDefault();
      }
    );
  }

}
