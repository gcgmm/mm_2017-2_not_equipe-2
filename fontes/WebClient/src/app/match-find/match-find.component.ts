import { Component, OnInit } from '@angular/core';
import { ViewDefault } from '../../common/view-default';
import { Match } from '../../model/match-model';
import { Game } from '../../model/game-model';
import {Router} from '@angular/router'
import { MatchService } from '../../geocirc-services/match.service';
import * as moment from 'moment';
@Component({
  selector: 'app-match-find',
  templateUrl: './match-find.component.html',
  styleUrls: ['./match-find.component.css'],
  providers: [MatchService]
})
export class MatchFindComponent extends ViewDefault implements OnInit {

  matchs: Array<Match>;
  
  constructor(private router: Router, private gameService: MatchService) {
    super();
    this.matchs = Array<Match>();
    this.getAllMatch();
  }

  ngOnInit() {
  }

  getAllMatch(){
    /*var v: Match = new Match();

    v.Id = 1;
    v.Descricao = "Match 01 kajdfkahgdkshjdgaskjhg"
    v.Jogo = new Game();
    v.Jogo.Descricao = "Jogo xxaaaaxx"

    this.matchs.push(v);
    v =  new Match();
    v.Id = 2;
    v.Descricao = "Match 02 kajdfkahgdasdfasdkshjdgaskjhg"
    v.Jogo = new Game();
    v.Jogo.Descricao = "Jogo asdasaxxxx"

    this.matchs.push(v);
    v =  new Match();
    v.Id = 1;
    v.Descricao = "Match 03 kajdfkaaashgdkshjdgaskjhg"
    v.Jogo = new Game();
    v.Jogo.Descricao = "Jogo asdsdxxxx"

    this.matchs.push(v);
    v =  new Match();
    v.Id = 1;
    v.Descricao = "Match 04 kajdfkaahgdkshjdgaskjhg"
    v.Jogo = new Game();
    v.Jogo.Descricao = "Jogo xasasdxxx"

    this.matchs.push(v);*/

    this.matchs = Array<Match>();
    super.setSuccessMessage("Carregando Partidas...");
    var status = this.gameService.getAllMatch();
    status.subscribe(
      response => {
        this.matchs = response;
        super.setSuccessMessage("Partidas Carregadas com Sucesso!");
      },
      error => {
        super.setErrorMessageDefault();
      }
    );
  }
 
  finalizar(match: Match){
    match.DataHoraFim = moment().format("YYYY/MM/DD HH:mm:ss");
    var status = this.gameService.updateMatch(match);
    status.subscribe(
      response => {
        this.getAllMatch();
      },
      error => {
        super.setErrorMessageDefault();
      }
    );
  }

  consultar(match: Match){
    this.router.navigate(['reports'], { queryParams: {matchId: match.Id} });
  }


}
