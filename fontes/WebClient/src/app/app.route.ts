import { Routes } from '@angular/router'

import { RouterModule }   from '@angular/router';
import { MenuComponent } from './menu/menu.component';
import { InicioComponent } from './inicio/inicio.component';
import { QuestionFormComponent } from './question-form/question-form.component';
import { QuestionFindComponent } from './question-find/question-find.component';
import { TipFormComponent } from './tip-form/tip-form.component';
import { TipFindComponent } from './tip-find/tip-find.component';
import { GameFormComponent } from './game-form/game-form.component';
import { GameFindComponent } from './game-find/game-find.component';
import { GameComponent } from './game/game.component';
import { MatchFormComponent } from './match-form/match-form.component';
import { MatchFindComponent } from './match-find/match-find.component';
import { ReportsComponent } from './reports/reports.component';

export const appRoutes: Routes = [
    {
        path: 'inicio',
        component: InicioComponent
    },
    {
        path: 'menu',
        component: MenuComponent
    },
    {
        path: 'gameForm',
        component: GameFormComponent
    },
    {
        path: 'gameFind',
        component: GameFindComponent
    },
    {
        path: 'questionForm',
        component: QuestionFormComponent
    },
    {
        path: 'questionFind',
        component: QuestionFindComponent
    },
    {
        path: 'tipForm',
        component: TipFormComponent
    },
    {
        path: 'tipFind',
        component: TipFindComponent
    },
    {
        path: 'matchForm',
        component: MatchFormComponent
    },
    {
        path: 'matchFind',
        component: MatchFindComponent
    },
    {
        path: 'reports',
        component: ReportsComponent
    },
    {
        path: 'game',
        component: GameComponent
    },
    {
        path: '',
        component: InicioComponent
    },
    {
        path: '**',
        component: InicioComponent
    },

]