import { Component, OnInit } from '@angular/core';
import { Params, ActivatedRoute } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { ViewChild } from '@angular/core';

import {TipService} from '../../geocirc-services/tip.service'

import {Tip} from '../../model/tip-model'
import { ViewDefault } from '../../common/view-default';
import * as moment from 'moment';

@Component({
  selector: 'app-tip-form',
  providers:[TipService],
  templateUrl: './tip-form.component.html',
  styleUrls: ['./tip-form.component.css']
})

export class TipFormComponent extends ViewDefault implements OnInit{
 
  private tip: Tip;

  constructor(private route: ActivatedRoute, private tipService: TipService) {
    super();
    this.tip = new Tip();
    this.clearLocalObject();
    this.route.queryParams.forEach(param => {
      if (param.tipId) {
        this.loadTip(param.tipId);
      }
    });
  }

  ngOnInit() {}

  clearLocalObject(){
    this.tip.Descricao = "";
    this.tip.Imagem = "";
    this.tip.TipoImagem = "";
  }

  saveObject(){    
    if (this.tip.Descricao == ""){
      super.setErrorMessage("Informe a Dica!");
    } else if (this.tip.Imagem == "") {
      super.setErrorMessage("Selecione uma Imagem!");
    } else if (this.tip.TipoImagem == "") {
      super.setErrorMessage("O tipo da Imagem não foi carregado.");
    } else {
      super.setSuccessMessage("Salvando dica...");
      var status;
      if (this.tip.Id == 0){
        status = this.tipService.insertTip(this.tip);
      } else {
        status = this.tipService.updateTip(this.tip);
      }
      status.subscribe(
        response => {
          super.setSuccessMessage("Dica " + this.tip.Descricao + " Cadastrada!");
          },
          error => {
            super.setErrorMessageDefault();
          }
      );
    }
  }

  loadTip(tipId){
    super.setSuccessMessage("Carregando Dica...");
    var status = this.tipService.loadTip(tipId);
    status.subscribe(
      response => {
        this.tip = response;
        super.setSuccessMessage("Dica Carregada com Sucesso!");
      },
      error => {
        super.setErrorMessageDefault();
      }
    );
  }

  onFileChange(event) {
    let reader = new FileReader();
    if(event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      reader.readAsDataURL(file);
      reader.onload = () => {
        if (file.type.split('/')[0] == 'image'){
          this.tip.TipoImagem = file.type;
          this.tip.Imagem = reader.result.split(',')[1];
          console.log("this.tip.TipoImagem " + this.tip.TipoImagem);
          console.log("this.tip.Imagem " + this.tip.Imagem);

        } else {
          super.setErrorMessage("O Arquivo Deve ser uma Imagem!");
        }
      };    
    }
  }

}
