import { Student } from "./student-model";

export class Island{
    
    public Id;
    public Descricao;
    public Aluno: Student;
    
    constructor(){
        this.Id = 0;
        this.Descricao = null;
        this.Aluno = new Student();
    }

}
