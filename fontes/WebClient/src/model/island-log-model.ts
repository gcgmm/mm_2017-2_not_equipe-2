import { Student } from "./student-model";

export class IslandLog {
    
    public Aluno: Student;
    public IdPartida: number;
    public IdPergunta: number;
    public DataHoraInicio: String;
    public DataHoraFim: String;
    public SequenciaResposta: number;
    public SequenciaIlha: number;

    constructor(){
        this.Aluno = null;
        this.IdPartida = null;
        this.IdPergunta = null;
        this.DataHoraInicio = null;
        this.DataHoraFim = null;
        this.SequenciaResposta = null;
        this.SequenciaIlha = null;
    }

}
