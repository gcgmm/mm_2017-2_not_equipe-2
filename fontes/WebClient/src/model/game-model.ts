
import {Question} from './question-model';

export class Game {

    public Id;
    public Descricao;
    public Perguntas: Array<Question>;
    
    constructor(){
        this.Id = 0;
        this.Descricao = null;
        this.Perguntas = null;
    }
}

export class QuestionGame {
    
        public IdJogo;
        public IdPergunta;
        
}