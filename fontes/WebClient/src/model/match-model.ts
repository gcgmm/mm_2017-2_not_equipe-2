import {Question} from './question-model';
import {Game} from './game-model';

import * as moment from 'moment';

export class Match {

    public Id;
    public Descricao;
    public DataHoraInicio: string;
    public DataHoraFim: string;
    public Jogo: Game;

    constructor(){
        this.Id = 0;
        this.Jogo = new Game();
        this.Descricao = null;
        this.DataHoraInicio = moment().format("YYYY/MM/DD HH:mm:ss");
        this.DataHoraFim = null;
    }
}

export var matchVar = {
    match: Match
}