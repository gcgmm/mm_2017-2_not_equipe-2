
export class Question {

    public Id;
    public Descricao;
    public ImagemCerta;
    public TipoImagemCerta;
    public Imagem1;
    public TipoImagem1;
    public Imagem2;
    public TipoImagem2;
    public Imagem3;
    public TipoImagem3;
    public Imagem4;
    public TipoImagem4;
    public IdDica;
   
    constructor(){
        this.Id = 0;
        this.Descricao = null;
        this.ImagemCerta = null;
        this.TipoImagemCerta = null;
        this.Imagem1 = null;
        this.TipoImagem1 = null;
        this.Imagem2 = null;
        this.TipoImagem2 = null;
        this.Imagem3 = null;
        this.TipoImagem3 = null;
        this.Imagem4 = null;
        this.TipoImagem4 = null;
        this.IdDica = null;
    }
   
}