import { Injectable } from '@angular/core';
import { Http, XSRFStrategy } from '@angular/http';
import { contentHeaders } from '../common/headers';
import { Observable } from 'rxjs/Rx'
import 'rxjs/add/operator/map'
import { environment } from '../environments/environment';
import {Match} from '../model/match-model'
import { retry } from 'rxjs/operator/retry';
import { Island } from '../model/island-model';
import { IslandLog } from '../model/island-log-model';

@Injectable()
export class MatchService {
  
    constructor(public http: Http) { }
    
    public insertMatch(partida: Match) : Observable<any>{
      let body = JSON.stringify(partida);
      return this.http.post(environment.server_url + '/Partida/', body, { headers: contentHeaders })
        .map(
          response => {
            return response.json();
          },
          error => {
            return error;
          }
        );
    }

    public updateMatch(partida: Match) : Observable<any>{
      let body = JSON.stringify(partida);
      return this.http.put(environment.server_url + '/Partida/' + partida.Id, body, { headers: contentHeaders })
        .map(
          response => {
            return response.json();
          },
          error => {
            return error;
          }
        );
    }

    public getMatch(id) : Observable<any>{
      return this.http.get(environment.server_url + '/Partida/' + id, { headers: contentHeaders })
        .map(
          response => {
            return response.json();
          },
          error => {
            return error;
          }
        );
    }

    public getIsland(id) : Observable<any>{
    return this.http.get(environment.server_url + '/Ilha/' + id, { headers: contentHeaders })
      .map(
        response => {
          return response.json();
        },
        error => {
          return error;
        }
      );
    }

  public getAllMatch() : Observable<any>{
    return this.http.get(environment.server_url + '/Partida/', { headers: contentHeaders })
      .map(
        response => {
          return response.json();
        },
        error => {
          return error;
        }
      );
    }

    public sendLog(islandLog: IslandLog) : Observable<any>{
      let body = JSON.stringify(islandLog);
      return this.http.post(environment.server_url + '/RespostaPartida/', body, { headers: contentHeaders })
        .map(
          response => {
            return response.json();
          },
          error => {
            return error;
          }
        );
      }
      public getMatchRancking(id) : Observable<any>{
        return this.http.get(environment.server_url + '/Relatorio/' + id, { headers: contentHeaders })
          .map(
            response => {
              return response.json();
            },
            error => {
              return error;
            }
          );
      }

}