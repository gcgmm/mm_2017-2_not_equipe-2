import { Injectable } from '@angular/core';
import { Http, XSRFStrategy } from '@angular/http';
import { contentHeaders } from '../common/headers';
import { Observable } from 'rxjs/Rx'
import 'rxjs/add/operator/map'
import { environment } from '../environments/environment';
import {Question} from '../model/question-model'

@Injectable()
export class QuestionService {

  constructor(public http: Http) { }
  
    public insertQuestion(questao: Question) : Observable<any>{
      let body = JSON.stringify(questao);
      return this.http.post(environment.server_url + '/Pergunta/', body, { headers: contentHeaders })
        .map(
          response => {
            return response.json();
          },
          error => {
            return error;
          }
        );
    }

    public updateQuestion(questao: Question) : Observable<any>{
      let body = JSON.stringify(questao);
      return this.http.put(environment.server_url + '/Pergunta/' + questao.Id, body, { headers: contentHeaders })
        .map(
          response => {
            return response.json();
          },
          error => {
            return error;
          }
        );
    }

    public loadQuestion(Id) : Observable<any>{
      return this.http.get(environment.server_url + '/Pergunta/' + Id, { headers: contentHeaders })
        .map(
          response => {
            return response.json();
          },
          error => {
            return error;
          }
        );
    }

    public deleteQuestion(Id) : Observable<any>{
      return this.http.delete(environment.server_url + '/Pergunta/' + Id, { headers: contentHeaders })
        .map(
          response => {
            return response.json();
          },
          error => {
            return error;
          }
        );
    }

    public getAllQuestions() : Observable<any>{
      return this.http.get(environment.server_url + '/Pergunta/', { headers: contentHeaders })
        .map(
          response => {
            return response.json();
          },
          error => {
            return error;
          }
        );
    }

}
