import { Injectable } from '@angular/core';
import { Http, XSRFStrategy } from '@angular/http';
import { contentHeaders } from '../common/headers';
import { Observable } from 'rxjs/Rx'
import 'rxjs/add/operator/map'
import { environment } from '../environments/environment';
import {Game, QuestionGame} from '../model/game-model'

@Injectable()
export class GameService {

  constructor(public http: Http) { }
  
  public insertGame(jogo: Game) : Observable<any>{
    let body = JSON.stringify(jogo);
    return this.http.post(environment.server_url + '/Jogo/', body, { headers: contentHeaders })
      .map(
        response => {
          return response.json();
        },
        error => {
          return error;
        }
      );
  }

  public updateGame(jogo: Game) : Observable<any>{
    let body = JSON.stringify(jogo);
    return this.http.put(environment.server_url + '/Jogo/' + jogo.Id, body, { headers: contentHeaders })
      .map(
        response => {
          return response.json();
        },
        error => {
          return error;
        }
      );
  }

  public loadGame(Id) : Observable<any>{
      return this.http.get(environment.server_url + '/Jogo/' + Id, { headers: contentHeaders })
        .map(
          response => {
            return response.json();
          },
          error => {
            return error;
          }
        );
  }

  public deleteGame(Id) : Observable<any>{
    return this.http.delete(environment.server_url + '/Jogo/' + Id, { headers: contentHeaders })
      .map(
        response => {
          return response.json();
        },
        error => {
          return error;
        }
      );
  }

  public getAllGames() : Observable<any>{
    return this.http.get(environment.server_url + '/Jogo/', { headers: contentHeaders })
      .map(
        response => {
          return response.json();
        },
        error => {
          return error;
        }
      );
  }

  public addQuestion(gameId: number, questionId: number){
    
    var obj = new QuestionGame();
    obj.IdJogo = gameId;
    obj.IdPergunta = questionId;
    let body = JSON.stringify(obj);

    return this.http.post(environment.server_url + '/PerguntaJogo/', body, { headers: contentHeaders })
      .map(
        response => {
          return response.json();
        },
        error => {
          return error;
        }
      );
  }

  public removeQuestion(gameId: number, questionId: number){
    
    var obj = new QuestionGame();
    obj.IdJogo = gameId;
    obj.IdPergunta = questionId;
    let body = JSON.stringify(obj);

    return this.http.put(environment.server_url + '/PerguntaJogo/', body, { headers: contentHeaders })
      .map(
        response => {
          return response.json();
        },
        error => {
          return error;
        }
      );
  }

}
