import { Injectable } from '@angular/core';
import { Http, XSRFStrategy } from '@angular/http';
import { contentHeaders } from '../common/headers';
import { Observable } from 'rxjs/Rx'
import 'rxjs/add/operator/map'
import { environment } from '../environments/environment';
import {Tip} from '../model/tip-model'

@Injectable()
export class TipService {

  constructor(public http: Http) { }
  
    public insertTip(dica: Tip) : Observable<any>{
      let body = JSON.stringify(dica);
      return this.http.post(environment.server_url + '/Dica/', body, { headers: contentHeaders })
        .map(
          response => {
            return response.json();
          },
          error => {
            return error;
          }
        );
    }

    public updateTip(tip: Tip) : Observable<any>{
      let body = JSON.stringify(tip);
      return this.http.put(environment.server_url + '/Dica/' + tip.Id, body, { headers: contentHeaders })
        .map(
          response => {
            return response.json();
          },
          error => {
            return error;
          }
        );
    }

    public loadTip(Id) : Observable<any>{
      return this.http.get(environment.server_url + '/Dica/' + Id, { headers: contentHeaders })
        .map(
          response => {
            return response.json();
          },
          error => {
            return error;
          }
        );
    }

    public deleteTip(Id) : Observable<any>{
      return this.http.delete(environment.server_url + '/Dica/' + Id, { headers: contentHeaders })
        .map(
          response => {
            return response.json();
          },
          error => {
            return error;
          }
        );
    }

    public getAllTips() : Observable<any>{
      return this.http.get(environment.server_url + '/Dica/', { headers: contentHeaders })
        .map(
          response => {
            return response.json();
          },
          error => {
            return error;
          }
        );
}

}
