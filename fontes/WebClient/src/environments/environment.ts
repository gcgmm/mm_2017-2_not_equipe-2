// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  //server_url: "http://localhost:62015/Api/"
  server_url: "http://10.9.32.8:62015/Api"
  //server_url: "http://25.6.130.4:62015/Api"
  //server_url: "http://192.168.137.1:62015/Api"
  //server_url: "http://192.168.43.135:62015/Api"
  //server_url: "http://201.54.201.141:62015/Api"
  //server_url: "http://10.4.37.248:62015/Api"
};
