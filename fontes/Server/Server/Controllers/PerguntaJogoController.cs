﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MySql.Data.MySqlClient;
using Server.Models;

namespace Server.Controllers
{
    public class PerguntaJogoController : ApiController
    {
        // GET: api/PerguntaJogo
        public List<PerguntaJogo> Get()
        {
            MySqlConnection connection = new MySqlConnection(Constants.myConnectionString);
            connection.Open();

            string query = "SELECT * FROM perguntas_jogo";
            MySqlCommand cmd = new MySqlCommand(query, connection);
            MySqlDataReader dataReader = cmd.ExecuteReader();

            List<PerguntaJogo> perguntas = new List<PerguntaJogo>();
            while (dataReader.Read())
                perguntas.Add(new PerguntaJogo(dataReader));

            dataReader.Close();
            connection.Close();

            return perguntas;
        }

        // GET: api/PerguntaJogo/5
        public List<int> Get(int id)
        {
            MySqlConnection connection = new MySqlConnection(Constants.myConnectionString);
            connection.Open();

            string query = "SELECT * FROM perguntas_jogo WHERE idjogo = " + id;
            MySqlCommand cmd = new MySqlCommand(query, connection);
            MySqlDataReader dataReader = cmd.ExecuteReader();

            List<int> perguntas = new List<int>();
            while (dataReader.Read())
                perguntas.Add(dataReader.GetInt32(1));

            dataReader.Close();
            connection.Close();

            return perguntas;
        }

        // POST: api/PerguntaJogo
        public void Post(PerguntaJogo perguntaJogo)
        {
            MySqlConnection connection = new MySqlConnection(Constants.myConnectionString);
            connection.Open();

            string query = "INSERT INTO perguntas_jogo VALUES("
                + perguntaJogo.IdJogo
                + ", "
                + perguntaJogo.IdPergunta
                + ")";

            MySqlCommand cmd = new MySqlCommand(query, connection);
            cmd.ExecuteReader();

            connection.Close();
        }

        // DELETE: api/PerguntaJogo
        public void Put(PerguntaJogo perguntaJogo)
        {
            MySqlConnection connection = new MySqlConnection(Constants.myConnectionString);
            connection.Open();

            string query = "DELETE FROM perguntas_jogo WHERE idjogo = " 
                + perguntaJogo.IdJogo 
                + " AND idPergunta = "
                + perguntaJogo.IdPergunta;

            MySqlCommand cmd = new MySqlCommand(query, connection);
            cmd.ExecuteReader();

            connection.Close();
        }
    }
}
