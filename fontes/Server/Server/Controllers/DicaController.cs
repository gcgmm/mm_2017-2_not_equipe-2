﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MySql.Data.MySqlClient;
using Server.Models;

namespace Server.Controllers
{
    public class DicaController : ApiController
    {
        // GET: api/Dica
        public IEnumerable<Dica> Get()
        {
            MySqlConnection connection = new MySqlConnection(Constants.myConnectionString);
            connection.Open();

            string query = "SELECT * FROM dicas";
            MySqlCommand cmd = new MySqlCommand(query, connection);
            MySqlDataReader dataReader = cmd.ExecuteReader();

            List<Dica> dicas = new List<Dica>();
            while (dataReader.Read())
                dicas.Add(new Dica(dataReader));

            dataReader.Close();
            connection.Close();

            return dicas;
        }

        // GET: api/Dica/5
        public Dica Get(int id)
        {
            MySqlConnection connection = new MySqlConnection(Constants.myConnectionString);
            connection.Open();

            string query = "SELECT * FROM dicas WHERE iddica = " + id;
            MySqlCommand cmd = new MySqlCommand(query, connection);
            MySqlDataReader dataReader = cmd.ExecuteReader();

            Dica dica = null;
            if (dataReader.Read())
                dica = new Dica(dataReader);

            dataReader.Close();
            connection.Close();

            return dica;
        }

        // POST: api/Dica
        public void Post(Dica dica)
        {
            MySqlConnection connection = new MySqlConnection(Constants.myConnectionString);
            connection.Open();

            string query = "INSERT INTO dicas VALUES(null, '"
                + dica.Descricao
                + "', '"
                + dica.TipoImagem
                + "', '"
                + dica.Imagem
                + "')";

            MySqlCommand cmd = new MySqlCommand(query, connection);
            cmd.ExecuteReader();

            connection.Close();
        }

        // PUT: api/Dica/5
        public void Put(int id, Dica dica)
        {
            MySqlConnection connection = new MySqlConnection(Constants.myConnectionString);
            connection.Open();

            string query = "UPDATE dicas SET descricao = '"
                + dica.Descricao
                + "', tipoImagem = '"
                + dica.TipoImagem
                + "', imagem = '"
                + dica.Imagem
                + "' WHERE iddica = "
                + id;

            MySqlCommand cmd = new MySqlCommand(query, connection);
            cmd.ExecuteReader();

            connection.Close();
        }

        // DELETE: api/Dica/5
        public void Delete(int id)
        {
            MySqlConnection connection = new MySqlConnection(Constants.myConnectionString);
            connection.Open();

            string query = "DELETE FROM dicas WHERE iddica = " + id;
            MySqlCommand cmd = new MySqlCommand(query, connection);
            cmd.ExecuteReader();

            connection.Close();
        }
    }
}
