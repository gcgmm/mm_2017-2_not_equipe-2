﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MySql.Data.MySqlClient;
using Server.Models;

namespace Server.Controllers
{
    public class JogoController : ApiController
    {
        // GET: api/Jogo
        public IEnumerable<Jogo> Get()
        {
            MySqlConnection connection = new MySqlConnection(Constants.myConnectionString);
            connection.Open();

            string query = "SELECT * FROM jogos";
            MySqlCommand cmd = new MySqlCommand(query, connection);
            MySqlDataReader dataReader = cmd.ExecuteReader();

            List<Jogo> jogos = new List<Jogo>();
            while (dataReader.Read())
            {
                Jogo jogo = new Jogo(dataReader);

                PerguntaJogoController perguntaJogoController = new PerguntaJogoController();
                List<int> perguntas = perguntaJogoController.Get(jogo.Id);

                PerguntaController perguntaController = new PerguntaController();
                for (int i = 0; i < perguntas.Count; ++i)
                    jogo.Perguntas.Add(perguntaController.Get(perguntas[i]));

                jogos.Add(jogo);
            }

            dataReader.Close();
            connection.Close();

            return jogos;
        }

        // GET: api/Jogo/5
        public Jogo Get(int id, bool bCompleto = true)
        {
            MySqlConnection connection = new MySqlConnection(Constants.myConnectionString);
            connection.Open();

            string query = "SELECT * FROM jogos WHERE idjogo = " + id;
            MySqlCommand cmd = new MySqlCommand(query, connection);
            MySqlDataReader dataReader = cmd.ExecuteReader();

            Jogo jogo = null;
            if (dataReader.Read())
            {
                jogo = new Jogo(dataReader);

                if (bCompleto)
                {
                    PerguntaJogoController perguntaJogoController = new PerguntaJogoController();
                    List<int> perguntas = perguntaJogoController.Get(jogo.Id);

                    PerguntaController perguntaController = new PerguntaController();
                    for (int i = 0; i < perguntas.Count; ++i)
                        jogo.Perguntas.Add(perguntaController.Get(perguntas[i]));
                }
            }

            dataReader.Close();
            connection.Close();

            return jogo;
        }

        // POST: api/Jogo
        public void Post(Jogo jogo)
        {
            MySqlConnection connection = new MySqlConnection(Constants.myConnectionString);
            connection.Open();

            string query = "INSERT INTO jogos VALUES(null, '"
                + jogo.Descricao
                + "')";

            MySqlCommand cmd = new MySqlCommand(query, connection);
            cmd.ExecuteReader();

            connection.Close();
        }

        // PUT: api/Jogo/5
        public void Put(int id, Jogo jogo)
        {
            MySqlConnection connection = new MySqlConnection(Constants.myConnectionString);
            connection.Open();

            string query = "UPDATE jogos SET descricao = '"
                + jogo.Descricao
                + "' WHERE idjogo = "
                + id;

            MySqlCommand cmd = new MySqlCommand(query, connection);
            cmd.ExecuteReader();

            connection.Close();
        }

        // DELETE: api/Jogo/5
        public void Delete(int id)
        {
            MySqlConnection connection = new MySqlConnection(Constants.myConnectionString);
            connection.Open();

            string query = "DELETE FROM jogos WHERE idjogo = " + id;
            MySqlCommand cmd = new MySqlCommand(query, connection);
            cmd.ExecuteReader();

            connection.Close();
        }
    }
}
