﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MySql.Data.MySqlClient;
using Server.Models;

namespace Server.Controllers
{
    public class PerguntaController : ApiController
    {

        // GET: api/Pergunta
        public IEnumerable<Pergunta> Get()
        {
            MySqlConnection connection = new MySqlConnection(Constants.myConnectionString);
            connection.Open();

            string query = "SELECT * FROM perguntas";
            MySqlCommand cmd = new MySqlCommand(query, connection);
            MySqlDataReader dataReader = cmd.ExecuteReader();

            List<Pergunta> perguntas = new List<Pergunta>();
            while (dataReader.Read())
                perguntas.Add(new Pergunta(dataReader));

            dataReader.Close();
            connection.Close();

            return perguntas;
        }

        // GET: api/Pergunta/5
        public Pergunta Get(int id)
        {
            MySqlConnection connection = new MySqlConnection(Constants.myConnectionString);
            connection.Open();

            string query = "SELECT * FROM perguntas WHERE idpergunta = " + id;
            MySqlCommand cmd = new MySqlCommand(query, connection);
            MySqlDataReader dataReader = cmd.ExecuteReader();

            Pergunta pergunta = null;
            if (dataReader.Read())
                pergunta = new Pergunta(dataReader);

            dataReader.Close();
            connection.Close();

            return pergunta;
        }

        // POST: api/Pergunta
        public void Post(Pergunta pergunta)
        {
            MySqlConnection connection = new MySqlConnection(Constants.myConnectionString);
            connection.Open();

            string query = "INSERT INTO perguntas VALUES(null, '" 
                + pergunta.Descricao 
                + "', '" 
                + pergunta.TipoImagem1 
                + "', '" 
                + pergunta.Imagem1 
                + "', '" 
                + pergunta.TipoImagem2 
                + "', '" 
                + pergunta.Imagem2
                + "', '"
                + pergunta.TipoImagem3
                + "', '"
                + pergunta.Imagem3
                + "', '"
                + pergunta.TipoImagem4
                + "', '"
                + pergunta.Imagem4
                + "', '"
                + pergunta.TipoImagemCerta
                + "', '"
                + pergunta.ImagemCerta
                + "', '"
                + pergunta.IdDica
                + "')";

            MySqlCommand cmd = new MySqlCommand(query, connection);
            cmd.ExecuteReader();

            connection.Close();
        }

        // PUT: api/Pergunta/5
        public void Put(int id, Pergunta pergunta)
        {
            MySqlConnection connection = new MySqlConnection(Constants.myConnectionString);
            connection.Open();

            string query = "UPDATE perguntas SET descricao = '"
                + pergunta.Descricao
                + "', TipoImagem1 = '"
                + pergunta.TipoImagem1
                + "', Imagem1 = '"
                + pergunta.Imagem1
                + "', TipoImagem2 = '"
                + pergunta.TipoImagem2
                + "', Imagem2 = '"
                + pergunta.Imagem2
                + "', TipoImagem3 = '"
                + pergunta.TipoImagem3
                + "', Imagem3 = '"
                + pergunta.Imagem3
                + "', TipoImagem4 = '"
                + pergunta.TipoImagem4
                + "', Imagem4 = '"
                + pergunta.Imagem4
                + "', TipoImagemCerta = '"
                + pergunta.TipoImagemCerta
                + "', ImagemCerta = '"
                + pergunta.ImagemCerta
                + "', IdDica = '"
                + pergunta.IdDica
                + "' WHERE idpergunta = "
                + id;

            MySqlCommand cmd = new MySqlCommand(query, connection);
            cmd.ExecuteReader();

            connection.Close();
        }

        // DELETE: api/Pergunta/5
        public void Delete(int id)
        {
            MySqlConnection connection = new MySqlConnection(Constants.myConnectionString);
            connection.Open();

            string query = "DELETE FROM perguntas WHERE idpergunta = " + id;
            MySqlCommand cmd = new MySqlCommand(query, connection);
            cmd.ExecuteReader();

            connection.Close();
        }
    }
}
