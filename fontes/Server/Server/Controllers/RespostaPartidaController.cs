﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MySql.Data.MySqlClient;
using Server.Models;

namespace Server.Controllers
{
    public class RespostaPartidaController : ApiController
    {
        // GET: api/RespostaPartida
        public IEnumerable<RespostaPartida> Get()
        {
            MySqlConnection connection = new MySqlConnection(Constants.myConnectionString);
            connection.Open();

            string query = "SELECT * FROM respostas_partida";
            MySqlCommand cmd = new MySqlCommand(query, connection);
            MySqlDataReader dataReader = cmd.ExecuteReader();

            AlunoController alunoController = new AlunoController();

            List<RespostaPartida> respostas = new List<RespostaPartida>();
            while (dataReader.Read())
            {
                RespostaPartida resposta = new RespostaPartida(dataReader);
                resposta.Aluno = alunoController.Get(resposta.Aluno.Id);

                respostas.Add(resposta);
            }

            dataReader.Close();
            connection.Close();

            return respostas;
        }

        // GET: api/RespostaPartida/4
        public IEnumerable<RespostaPartida> Get(int idPartida)
        {
            MySqlConnection connection = new MySqlConnection(Constants.myConnectionString);
            connection.Open();

            string query = "SELECT * FROM respostas_partida WHERE idpartida = " + idPartida;
            MySqlCommand cmd = new MySqlCommand(query, connection);
            MySqlDataReader dataReader = cmd.ExecuteReader();

            AlunoController alunoController = new AlunoController();

            List<RespostaPartida> respostas = new List<RespostaPartida>();
            while (dataReader.Read())
            {
                RespostaPartida resposta = new RespostaPartida(dataReader);
                resposta.Aluno = alunoController.Get(resposta.Aluno.Id);

                respostas.Add(resposta);
            }

            dataReader.Close();
            connection.Close();

            return respostas;
        }

        // POST: api/RespostaPartida
        public void Post(RespostaPartida resposta)
        {
            MySqlConnection connection = new MySqlConnection(Constants.myConnectionString);
            connection.Open();

            string query = "INSERT INTO respostas_partida VALUES("
                + resposta.IdPartida
                + ","
                + resposta.IdPergunta
                + ","
                + resposta.Aluno.Id
                + ","
                + resposta.SequenciaResposta
                + ","
                + resposta.SequenciaIlha
                + ",'"
                + resposta.DataHoraInicio
                + "','"
                + resposta.DataHoraFim
                + "')";

            MySqlCommand cmd = new MySqlCommand(query, connection);
            cmd.ExecuteReader();

            connection.Close();

            if (resposta.SequenciaResposta != 0)
            {
                IlhaController ilhaController = new IlhaController();
                ilhaController.Delete(resposta.SequenciaIlha);
            }
        }
    }
}
