﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MySql.Data.MySqlClient;
using Server.Models;

namespace Server.Controllers
{
    public class RelatorioController : ApiController
    {
        // GET: api/Relatorio/5
        public IEnumerable<Relatorio> Get(int id)
        {
            MySqlConnection connection = new MySqlConnection(Constants.myConnectionString);
            connection.Open();

            string query = ""
            + "SELECT"
            + "    alu.nomeGoogle AS nome,"
            + "    SUM(CASE WHEN sequenciaResposta = 0 THEN 1 ELSE 0 END) AS corretas, "
            + "    SUM(CASE WHEN sequenciaResposta = 0 THEN 0 ELSE 1 END) AS erradas,"
            + "    (SELECT COUNT(*) FROM dicalog AS dica WHERE dica.idpartida = resp.idpartida AND dica.idaluno = resp.idaluno) AS dicas"
            + "    FROM"
            + "    	respostas_partida AS resp"
            + "        JOIN"
            + "        alunos AS alu"
            + "        ON"
            + "    		alu.idaluno = resp.idaluno"
            + "    WHERE"
            + "    	idpartida = " + id
            + "    GROUP BY"
            + "    	resp.idaluno"
            + "    ORDER BY"
            + "    	corretas DESC,"
            + "        erradas ASC,"
            + "        dicas ASC;"
            ;

            MySqlCommand cmd = new MySqlCommand(query, connection);
            MySqlDataReader dataReader = cmd.ExecuteReader();

            List<Relatorio> list = new List<Relatorio>();
            while (dataReader.Read())
                list.Add(new Relatorio(dataReader));

            dataReader.Close();
            connection.Close();

            return list;
        }
    }
}
