﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MySql.Data.MySqlClient;
using Server.Models;

namespace Server.Controllers
{
    public class PartidaController : ApiController
    {
        // GET: api/Partida
        public IEnumerable<Partida> Get()
        {
            MySqlConnection connection = new MySqlConnection(Constants.myConnectionString);
            connection.Open();

            string query = "SELECT * FROM partidas";
            MySqlCommand cmd = new MySqlCommand(query, connection);
            MySqlDataReader dataReader = cmd.ExecuteReader();

            JogoController jogoController = new JogoController();

            List<Partida> partidas = new List<Partida>();
            while (dataReader.Read())
            {
                Partida partida = new Partida(dataReader);
                partida.Jogo = jogoController.Get(partida.Jogo.Id, false);

                partidas.Add(partida);
            }

            dataReader.Close();
            connection.Close();

            return partidas;
        }

        // GET: api/Partida/5
        public Partida Get(int id, bool bCompleto = false)
        {
            MySqlConnection connection = new MySqlConnection(Constants.myConnectionString);
            connection.Open();

            string query = "SELECT * FROM partidas WHERE idpartida = " + id;
            MySqlCommand cmd = new MySqlCommand(query, connection);
            MySqlDataReader dataReader = cmd.ExecuteReader();

            JogoController jogoController = new JogoController();

            Partida partida = null;
            if (dataReader.Read())
            {
                partida = new Partida(dataReader);
                partida.Jogo = jogoController.Get(partida.Jogo.Id, bCompleto);
            }

            dataReader.Close();
            connection.Close();

            return partida;
        }

        // POST: api/Partida
        public Partida Post(Partida partida)
        {
            MySqlConnection connection = new MySqlConnection(Constants.myConnectionString);
            connection.Open();

            string query = "INSERT INTO partidas VALUES(null, "
                + partida.Jogo.Id
                + ",'"
                + partida.Descricao
                + "','"
				+ partida.DataHoraInicio
				+ "',"
				+ (partida.DataHoraFim == null ? "null" : ("'" + partida.DataHoraFim + "'"))
                + ")";

            MySqlCommand cmd = new MySqlCommand(query, connection);
            cmd.ExecuteReader();

            connection.Close();

            // TODO: remover todas as ilhas

            return Get((int)cmd.LastInsertedId, true);
        }

        // PUT: api/Partida
        public void Put(int id, Partida partida)
        {
            MySqlConnection connection = new MySqlConnection(Constants.myConnectionString);
            connection.Open();

            string query = "UPDATE partidas SET datahorafim = '"
                + partida.DataHoraFim
                + "' "
                + "WHERE idpartida = "
                + id;

            MySqlCommand cmd = new MySqlCommand(query, connection);
            cmd.ExecuteReader();
            connection.Close();
        }
    }
}
