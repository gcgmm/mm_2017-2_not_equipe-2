﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MySql.Data.MySqlClient;
using Server.Models;

namespace Server.Controllers
{
    public class IlhaController : ApiController
    {
        // GET: api/Ilha
        public IEnumerable<Ilha> Get()
        {
            MySqlConnection connection = new MySqlConnection(Constants.myConnectionString);
            connection.Open();

            string query = "SELECT * FROM ilhas";
            MySqlCommand cmd = new MySqlCommand(query, connection);
            MySqlDataReader dataReader = cmd.ExecuteReader();

            AlunoController alunoController = new AlunoController();

            List<Ilha> ilhas = new List<Ilha>();
            while (dataReader.Read())
            {
                Ilha ilha = new Ilha(dataReader);
                ilha.Aluno = alunoController.Get(ilha.Aluno.Id);

                ilhas.Add(ilha);
            }

            dataReader.Close();
            connection.Close();

            return ilhas;
        }

        // GET: api/Ilha/5
        public Ilha Get(int id)
        {
            MySqlConnection connection = new MySqlConnection(Constants.myConnectionString);
            connection.Open();

            string query = "SELECT * FROM ilhas WHERE idilha = " + id;
            MySqlCommand cmd = new MySqlCommand(query, connection);
            MySqlDataReader dataReader = cmd.ExecuteReader();

            Ilha ilha = null;
            if (dataReader.Read())
            {
                ilha = new Ilha(dataReader);

                AlunoController alunoController = new AlunoController();
                ilha.Aluno = alunoController.Get(ilha.Aluno.Id);
            }

            dataReader.Close();
            connection.Close();

            return ilha;
        }

        // POST: api/Ilha
        public void Post(Ilha ilha)
        {
            MySqlConnection connection = new MySqlConnection(Constants.myConnectionString);
            connection.Open();

            AlunoController alunoController = new AlunoController();
            Aluno aluno = alunoController.Get(ilha.Aluno.EmailGoogle);
            if (aluno == null)
                ilha.Aluno.Id = alunoController.Post(ilha.Aluno);
            else
                ilha.Aluno = aluno;

            string query = "";
            if (Get(ilha.Id) == null)
            {
                query = "INSERT INTO ilhas VALUES("
                    + ilha.Id
                    + ", "
                    + ilha.Aluno.Id
                    + ")";
            }
            else
            {
                query = "UPDATE ilhas SET idaluno = "
                    + ilha.Aluno.Id
                    + " WHERE idilha = "
                    + ilha.Id;
            }

            MySqlCommand cmd = new MySqlCommand(query, connection);
            cmd.ExecuteReader();

            connection.Close();
        }

        // DELETE: api/Ilha/5
        public void Delete(int id)
        {
            MySqlConnection connection = new MySqlConnection(Constants.myConnectionString);
            connection.Open();

            string query = "DELETE FROM ilhas WHERE idilha = " + id;
            MySqlCommand cmd = new MySqlCommand(query, connection);
            cmd.ExecuteReader();

            connection.Close();
        }
    }
}
