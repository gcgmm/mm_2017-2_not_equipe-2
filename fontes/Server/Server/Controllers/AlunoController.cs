﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MySql.Data.MySqlClient;
using Server.Models;

namespace Server.Controllers
{
    public class AlunoController : ApiController
    {
        // GET: api/Aluno
        public IEnumerable<Aluno> Get()
        {
            MySqlConnection connection = new MySqlConnection(Constants.myConnectionString);
            connection.Open();

            string query = "SELECT * FROM alunos";
            MySqlCommand cmd = new MySqlCommand(query, connection);
            MySqlDataReader dataReader = cmd.ExecuteReader();

            List<Aluno> alunos = new List<Aluno>();
            while (dataReader.Read())
                alunos.Add(new Aluno(dataReader));

            dataReader.Close();
            connection.Close();

            return alunos;
        }

        // GET: api/Aluno/5
        public Aluno Get(int id)
        {
            MySqlConnection connection = new MySqlConnection(Constants.myConnectionString);
            connection.Open();

            string query = "SELECT * FROM alunos WHERE idaluno = " + id;
            MySqlCommand cmd = new MySqlCommand(query, connection);
            MySqlDataReader dataReader = cmd.ExecuteReader();

            Aluno aluno = null;
            if (dataReader.Read())
                aluno = new Aluno(dataReader);

            dataReader.Close();
            connection.Close();

            return aluno;
        }

        // GET: api/Aluno
        public Aluno Get(string emailGoogle)
        {
            MySqlConnection connection = new MySqlConnection(Constants.myConnectionString);
            connection.Open();

            string query = "SELECT * FROM alunos WHERE emailGoogle = '" + emailGoogle + "'";
            MySqlCommand cmd = new MySqlCommand(query, connection);
            MySqlDataReader dataReader = cmd.ExecuteReader();

            Aluno aluno = null;
            if (dataReader.Read())
                aluno = new Aluno(dataReader);

            dataReader.Close();
            connection.Close();

            return aluno;
        }

        // POST: api/Aluno
        public int Post(Aluno aluno)
        {
            MySqlConnection connection = new MySqlConnection(Constants.myConnectionString);
            connection.Open();

            string query = "INSERT INTO alunos VALUES(null, '" + aluno.NomeGoogle + "', '" + aluno.EmailGoogle + "')";
            MySqlCommand cmd = new MySqlCommand(query, connection);
            cmd.ExecuteReader();

            connection.Close();
            return (int)cmd.LastInsertedId;
        }
    }
}
