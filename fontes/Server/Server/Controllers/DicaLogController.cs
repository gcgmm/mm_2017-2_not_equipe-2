﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MySql.Data.MySqlClient;
using Server.Models;

namespace Server.Controllers
{
    public class DicaLogController : ApiController
    {
        // GET: api/DicaLog
        public IEnumerable<DicaLog> Get()
        {
            MySqlConnection connection = new MySqlConnection(Constants.myConnectionString);
            connection.Open();

            string query = "SELECT * FROM dicalog";
            MySqlCommand cmd = new MySqlCommand(query, connection);
            MySqlDataReader dataReader = cmd.ExecuteReader();

            AlunoController alunoController = new AlunoController();

            List<DicaLog> logs = new List<DicaLog>();
            while (dataReader.Read())
            {
                DicaLog log = new DicaLog(dataReader);
                log.Aluno = alunoController.Get(log.Aluno.Id);

                logs.Add(log);
            }

            dataReader.Close();
            connection.Close();

            return logs;
        }

        // POST: api/DicaLog
        public Dica Post(DicaLog dicaLog)
        {
            MySqlConnection connection = new MySqlConnection(Constants.myConnectionString);
            connection.Open();

            AlunoController alunoController = new AlunoController();
            Aluno aluno = alunoController.Get(dicaLog.Aluno.EmailGoogle);
            if (aluno == null)
                dicaLog.Aluno.Id = alunoController.Post(dicaLog.Aluno);
            else
                dicaLog.Aluno = aluno;

            string query = "INSERT INTO dicalog VALUES("
                + dicaLog.IdDica
                + ","
                + dicaLog.IdPartida
                + ","
                + dicaLog.Aluno.Id
                + ")";

            MySqlCommand cmd = new MySqlCommand(query, connection);
            cmd.ExecuteReader();

            connection.Close();

            DicaController dicaController = new DicaController();
            return dicaController.Get(dicaLog.IdDica);
        }
    }
}
