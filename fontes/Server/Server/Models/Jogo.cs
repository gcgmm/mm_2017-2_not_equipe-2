﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;

namespace Server.Models
{
    public class Jogo
    {
        public int Id { get; set; }
        public string Descricao { get; set; }

        public List<Pergunta> Perguntas { get; set; } = new List<Pergunta>();

        public Jogo()
        {

        }

        public Jogo(MySqlDataReader dataReader)
        {
            Id = dataReader.GetInt32(0);
            Descricao = dataReader.GetString(1);
        }
    }
}