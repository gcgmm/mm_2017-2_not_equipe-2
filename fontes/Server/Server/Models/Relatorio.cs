﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;

namespace Server.Models
{
    public class Relatorio
    {
        public string Nome { get; set; }
        public int Corretas { get; set; }
        public int Erradas { get; set; }
        public int Dicas { get; set; }

        public Relatorio(MySqlDataReader dataReader)
        {
            Nome = dataReader.GetString(0);
            Corretas = dataReader.GetInt32(1);
            Erradas = dataReader.GetInt32(2);
            Dicas = dataReader.GetInt32(3);
        }
    }
}