﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;

namespace Server.Models
{
    public class Aluno
    {
        public int Id { get; set; }
        public string NomeGoogle { get; set; }
        public string EmailGoogle { get; set; }

        public Aluno()
        {

        }

        public Aluno(MySqlDataReader dataReader)
        {
            Id = dataReader.GetInt32(0);
            NomeGoogle = dataReader.GetString(1);
            EmailGoogle = dataReader.GetString(2);
        }
    }
}