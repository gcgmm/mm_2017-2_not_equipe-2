﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;

namespace Server.Models
{
    public class RespostaPartida
    {
        public int IdPartida { get; set; }
        public int IdPergunta { get; set; }
        public Aluno Aluno { get; set; } = new Aluno();
        public int SequenciaResposta { get; set; }
        public int SequenciaIlha { get; set; }
        public string DataHoraInicio { get; set; }
        public string DataHoraFim { get; set; }

        public RespostaPartida()
        {
        }

        public RespostaPartida(MySqlDataReader dataReader)
        {
            IdPartida = dataReader.GetInt32(0);
            IdPergunta = dataReader.GetInt32(1);
            Aluno.Id = dataReader.GetInt32(2);
            SequenciaResposta = dataReader.GetInt32(3);
            SequenciaIlha = dataReader.GetInt32(4);
            DataHoraInicio = dataReader.GetString(5);
            DataHoraFim = dataReader.GetString(6);
        }
    }
}