﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;

namespace Server.Models
{
    public class Partida
    {
        public int Id { get; set; }
        public Jogo Jogo { get; set; } = new Jogo();
        public string Descricao { get; set; }
		public string DataHoraInicio { get; set; }
		public string DataHoraFim { get; set; }

        public Partida()
        {

        }

        public Partida(MySqlDataReader dataReader)
        {
            Id = dataReader.GetInt32(0);
            Jogo.Id = dataReader.GetInt32(1);
            Descricao = dataReader.GetString(2);
            DataHoraInicio = dataReader.GetString(3);
            DataHoraFim = dataReader.IsDBNull(4) ? "" : dataReader.GetString(4);
        }
    }
}