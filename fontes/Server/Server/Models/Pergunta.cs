﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;

namespace Server.Models
{
    public class Pergunta
    {
        public int Id { get; set; }
        public string Descricao { get; set; }
        public string TipoImagem1 { get; set; }
        public string Imagem1 { get; set; }
        public string TipoImagem2 { get; set; }
        public string Imagem2 { get; set; }
        public string TipoImagem3 { get; set; }
        public string Imagem3 { get; set; }
        public string TipoImagem4 { get; set; }
        public string Imagem4 { get; set; }
        public string TipoImagemCerta { get; set; }
        public string ImagemCerta { get; set; }
        public int IdDica { get; set; }

        public Pergunta()
        {

        }

        public Pergunta(MySqlDataReader dataReader)
        {
            Id = dataReader.GetInt32(0);
            Descricao = dataReader.GetString(1);
            TipoImagem1 = dataReader.GetString(2);
            Imagem1 = dataReader.GetString(3);
            TipoImagem2 = dataReader.GetString(4);
            Imagem2 = dataReader.GetString(5);
            TipoImagem3 = dataReader.GetString(6);
            Imagem3 = dataReader.GetString(7);
            TipoImagem4 = dataReader.GetString(8);
            Imagem4 = dataReader.GetString(9);
            TipoImagemCerta = dataReader.GetString(10);
            ImagemCerta = dataReader.GetString(11);
            IdDica = dataReader.GetInt32(12);
        }

    }
}