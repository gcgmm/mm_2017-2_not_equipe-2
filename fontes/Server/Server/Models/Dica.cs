﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;

namespace Server.Models
{
    public class Dica
    {
        public int Id { get; set; }
        public string Descricao { get; set; }
        public string TipoImagem { get; set; }
        public string Imagem { get; set; }

        public Dica()
        {

        }

        public Dica(MySqlDataReader dataReader)
        {
            Id = dataReader.GetInt32(0);
            Descricao = dataReader.GetString(1);
            TipoImagem = dataReader.GetString(2);
            Imagem = dataReader.GetString(3);
        }
    }
}