﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;

namespace Server.Models
{
    public class PerguntaJogo
    {
        public int IdJogo { get; set; }
        public int IdPergunta { get; set; }

        public PerguntaJogo()
        {

        }

        public PerguntaJogo(MySqlDataReader dataReader)
        {
            IdJogo = dataReader.GetInt32(0);
            IdPergunta = dataReader.GetInt32(1);
        }
    }
}