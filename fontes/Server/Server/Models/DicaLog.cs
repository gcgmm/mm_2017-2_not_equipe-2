﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;

namespace Server.Models
{
    public class DicaLog
    {
        public int IdDica { get; set; }
        public int IdPartida { get; set; }
        public Aluno Aluno { get; set; } = new Aluno();

        public DicaLog()
        {

        }

        public DicaLog(MySqlDataReader dataReader)
        {
            IdDica = dataReader.GetInt32(0);
            IdPartida = dataReader.GetInt32(1);
            Aluno.Id = dataReader.GetInt32(2);
        }
    }
}