﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;

namespace Server.Models
{
    public class Ilha
    {
        public int Id { get; set; }
        public Aluno Aluno { get; set; } = new Aluno();

        public Ilha()
        {

        }

        public Ilha(MySqlDataReader dataReader)
        {
            Id = dataReader.GetInt32(0);
            Aluno.Id = dataReader.GetInt32(1);
        }
    }
}