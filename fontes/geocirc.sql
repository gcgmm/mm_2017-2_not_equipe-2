/*
DROP DATABASE if exists geocirc;
CREATE DATABASE `geocirc`;
*/
USE geocirc;

CREATE TABLE `alunos` (
  `idaluno` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `nomeGoogle` LONGTEXT NOT NULL,
  `emailGoogle` VARCHAR(200) NOT NULL,
  PRIMARY KEY (`idaluno`, `emailGoogle`),
  UNIQUE INDEX `emailGoogle_UNIQUE` (`emailGoogle` ASC),
  UNIQUE INDEX `idaluno_UNIQUE` (`idaluno` ASC));

CREATE TABLE `dicas` (
  `iddica` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `descricao` LONGTEXT NOT NULL,
  `tipoImagem` LONGTEXT NOT NULL,
  `imagem` LONGTEXT NOT NULL,
  PRIMARY KEY (`iddica`),
  UNIQUE INDEX `iddica_UNIQUE` (`iddica` ASC));
  
CREATE TABLE `perguntas` (
  `idpergunta` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `descricao` LONGTEXT NOT NULL,
  `tipoImagem1` LONGTEXT NOT NULL,
  `imagem1` LONGTEXT NOT NULL,
  `tipoImagem2` LONGTEXT NOT NULL,
  `imagem2` LONGTEXT NOT NULL,
  `tipoImagem3` LONGTEXT NOT NULL,
  `imagem3` LONGTEXT NOT NULL,
  `tipoImagem4` LONGTEXT NOT NULL,
  `imagem4` LONGTEXT NOT NULL,
  `tipoImagemCerta` LONGTEXT NOT NULL,
  `imagemCerta` LONGTEXT NOT NULL,
  `iddica` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`idpergunta`),
  CONSTRAINT FOREIGN KEY (`iddica`) REFERENCES `dicas`(`iddica`),
  UNIQUE INDEX `idpergunta_UNIQUE` (`idpergunta` ASC));
  

CREATE TABLE `jogos` (
  `idjogo` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `descricao` LONGTEXT NOT NULL,
  PRIMARY KEY (`idjogo`),
  UNIQUE INDEX `idjogo_UNIQUE` (`idjogo` ASC));
    
  CREATE TABLE `perguntas_jogo` (
  `idjogo` INT UNSIGNED NOT NULL,
  `idpergunta` INT UNSIGNED NOT NULL,
  CONSTRAINT FOREIGN KEY (`idjogo`) REFERENCES `jogos`(`idjogo`) ON DELETE CASCADE,
  CONSTRAINT FOREIGN KEY (`idpergunta`) REFERENCES `perguntas`(`idpergunta`)  ON DELETE CASCADE);
  
  

  CREATE TABLE `partidas` (
  `idpartida` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `idjogo` INT UNSIGNED NOT NULL,
  `descricao` LONGTEXT NOT NULL,
  `datahorainicio` DATETIME,
  `datahorafim` DATETIME,
  PRIMARY KEY (`idpartida`),
  UNIQUE INDEX `idpartida_UNIQUE` (`idpartida` ASC),
  CONSTRAINT FOREIGN KEY (`idjogo`) REFERENCES `jogos`(`idjogo`) ON DELETE CASCADE);
  
  
  CREATE TABLE `respostas_partida` (
  `idpartida` INT UNSIGNED NOT NULL,
  `idpergunta` INT UNSIGNED NOT NULL,
  `idaluno` INT UNSIGNED NOT NULL,
  `sequenciaResposta` INT UNSIGNED NOT NULL, /*(1-5, sendo 1 a certa)*/
  `sequenciaIlha` INT UNSIGNED NOT NULL, /*(1-4, sentido horário começando pela ilha de baixo)*/
  `datahorainicio` DATETIME,
  `datahorafim` DATETIME,
  CONSTRAINT FOREIGN KEY (`idpartida`) REFERENCES `partidas`(`idpartida`),
  CONSTRAINT FOREIGN KEY (`idpergunta`) REFERENCES `perguntas`(`idpergunta`),
  CONSTRAINT FOREIGN KEY (`idaluno`) REFERENCES `alunos`(`idaluno`));
  
  
  CREATE TABLE `ilhas` (
  `idilha` INT UNSIGNED NOT NULL,
  `idaluno` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`idilha`),
  UNIQUE INDEX `idilha_UNIQUE` (`idilha` ASC),
  CONSTRAINT FOREIGN KEY (`idaluno`) REFERENCES `alunos`(`idaluno`));
  

  CREATE TABLE `dicalog` (
  `iddica` INT UNSIGNED NOT NULL,
  `idpartida` INT UNSIGNED NOT NULL,
  `idaluno` INT UNSIGNED NOT NULL,
  CONSTRAINT FOREIGN KEY (`iddica`) REFERENCES `dicas`(`iddica`),
  CONSTRAINT FOREIGN KEY (`idpartida`) REFERENCES `partidas`(`idpartida`),
  CONSTRAINT FOREIGN KEY (`idaluno`) REFERENCES `alunos`(`idaluno`));