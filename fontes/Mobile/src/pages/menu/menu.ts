import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { GooglePlus } from '@ionic-native/google-plus';
import { QRModel, Login, Aluno, Dica, DicaExibir, DicaExibirGlobal } from '../QRModel';

import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';
import { contentHeaders } from '../header';

import 'rxjs/add/operator/map';

/**
 * Generated class for the MenuPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-menu',
  templateUrl: 'menu.html',
})
export class MenuPage {
  dicaGlobal;

  constructor(public navCtrl: NavController, public navParams: NavParams, private barcodeScanner: BarcodeScanner,
     private googlePlus: GooglePlus, private http: Http) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MenuPage');
  }

  openLerQR(){
    this.barcodeScanner.scan().then((barcodeData) => {
      //alert(barcodeData.text)
      var nome:QRModel = JSON.parse(barcodeData.text)
      if (nome.Type == 'L'){
        var login:Login = new Login();
        login.Id = nome.Id;
        login.Aluno = new Aluno();

        login.Aluno.NomeGoogle = localStorage.getItem('Nome');
        login.Aluno.EmailGoogle = localStorage.getItem('Email');

        
        let body = JSON.stringify(login);

        this.http.post('http://10.9.32.8:62015/Api/Ilha', body, { headers: contentHeaders }).map(
        response => {
        },
        error => {
        }
      ).subscribe(response => {
        //alert ('Acertou!');
        //console.log('Acertou');
      },
      error => {
        alert ('Ocorreu um erro ao ler o QR Code!');
      });

      } else {
        //'D' Dica
        //alert('entrei no else');
        this.dicaGlobal = new DicaExibir();
        this.dicaGlobal.Descricao = 'Descrição';  
        this.dicaGlobal.TipoImagem = 'image/jpeg';
        this.dicaGlobal.Imagem = 'iVBORw0KGgoAAAANSUhEUgAAAU0AAABkCAIAAACNRvXZAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAAEnQAABJ0Ad5mH3gAAAwFSURBVHhe7Z1fiF1HHcfPg48SUGr61IdSBCslD80VvAgKVQv6IvShsrgWa+WCUHyp2Ly4NYvSUOgl1qg0Uo2Lu8UUrYK9lpg8+LKJpFUKtukGY1vLJrt9WKRgtEFZf/P/N+fMnHvm7t5zz518P0xu5vzmNzO/M2e+d87unju32N76x97TqVOndgEAXQU6ByB/oHMA8gc6ByB/oHMA8gc6ByB/oHMA8gc6ByB/EnS+dW1lsZD0lkpF0DkAXSZB52uLxeLqW5Q5f/Rw7+gfeRF0DkCXSdL54eV1ofOt9aXe4govgs4B6DIp9+0kb3XfXmjB2wSdA9BlEnR+/uiCXs+vrazJG3iboHMAukyCzpd7C2vXtM4Xcd8OwPyQ9PM5dA7AXJLy8/nqgv7xvNC/eLcJOgegyyTovCZB5wB0GegcgPyBzgHIH+gcgPyBzgHIH+gcgPyBzgHIH+gcgPyZrs4H49B+AIBpMnWd61wI6ByAdkjT+bL+YKp+0N2mFnQ+Gmz2hzf0QZe5POwXg5E+APuMGF1Ff+gOKJ+Ia8eS3sgckaBzu42MeNC92edY5n09H/Z3slTssN+td6KkeEii1ns0KCZ+U6V2+sPL/EDn94/ujHOCzu1+MpTsZ1RVmqrOLw+3i+IqJbeeX363X2wW/e0BvRY7o9GOdBCaHPbJsj0cKgulbXspCVmq7E7AykiNm45kkepCO4s09orxFcJNIEJMRo21W5uwsAOvrNROjJC/DsYErV34GmgYPxN5FSOGuvhD/spGV1yZXbfp8VAN5SNqcu9Qv8aoB8Yeqby0jgbWub3zUk3byzV1Jl3P/S1lxuqcMhxrVJkmkAi9+3apQzF2QuRCmeRgLr8Uv/SSbjpPdtcC1eq/q/OEeqdQltGOvSQTrudi5rALOBrZRuji28bNPJPQdbcFEf8oQX9q3a/Jg0paZ0rLHp/iwfhr/O1s9+omxkN1ByOhE94CEeu3dDlsXzIeifWUeLFN67w6rHO332txuNdL03mQvercyLIYXBcGpnM+utpO/tLNMhqwpX604zVu2B+duwklYLG5SeRNj6h/hIi/mmSiTMxIb9VK0JXfuMTWDcUf96cS12tEe01QPVAF+t81Uhcna5/1SzVk3hsZyWzOa6qk/R7Opjbv2xUpOvfu1Wetc5o07sibE/bQzSuizj9E1F9GIV7kP6+h8a06RG2drRCOP+LvnYsMTucT9cDbYdm6OG13vCNqh8fAmcl5TZVJdN58X+dZref8ntxq1df/9QHzievcVBE39o01zy+2nE0uz1cZgXD15ttYf7aSCGr8RzTAMhLqpO/NONejaI+XVNqXJj53fSrxx/0pOh4b90mKp9SOLayNk7oQN/vcQY4cj8Hvd6rnpU6r5o1pv0n7+VwERzT+nob90Pl1+cs29/sw0tsLJHJlJIXLH61JpaRzytBYkjgHA/2rO1WkW7K1ZDKjXm6fX23VuEwpC3tlPulxK/qkO/qPdyFKvS7r/dUE8SpE/UWBCqQyRVUzAr/vUPuE60FQ6b3sHvC3QYoD07uLqXE87mS1SKSDEUxNnNIx1I7D85/qeUlzpfVpMuF9eynV6Lwe7bevlO7b24euva8q0AH8N9+ZQjJvO5bp6rzElITNUX8kE4nflreBe+u2ywvoAuzCtLuGdoncdA4AqAKdA5A/0DkA+QOdA5A/0DkA+QOdA5A/0DkA+QOdA5A/YZ2bj6aV941Z0x9Ya7qfTIm96Bz7yQDCPa/a39N+Mt7TM6HrNYNn1qZJWOfLPfGNqKUPpYnn2+U2MlurCw0/x1Ji7tZz7CfTDknxkLqtt1DrHk5lv56F7dp4Vqm7by/p3B6K1d7/KMtUda4+oEKp/IkU7CejCPnrYEzQ2oWvgYbxM5RXMStnXfwhf2WjCaDMrtv0eKiG8hE1uXeoX2PUA2OPZBkFHhhg20y1zHXQJyonK3ERheJRtsA4ECF/QlkpGFPOz1kNfeAsSkyi82pRC+t54HOp2E9Ggf1kJLF+S5eD91WznpciJLhFda/yRDD+1HGIxk8YOas868wUeJEGmGedG1liP5mgv5p8okzMDxGTLjBFjfAbl9i6ofjj/lTieo1rbyyqB6pA/7tG6uJk7fv9Julcnq/FCzgQf+o41MZfF2gz5uC+XZGic+9efdY6p8nhjrxrbA+dXog6/xBRfxmFeJH/vIbGt+oQtXW2Qjj+iL93LjI4nU+Lx2uHZevitN2VOuJRlIaaDnmEZeSo63w4/tRxaBT/xCTo/PzRBbUn3Nb60mLrv4dL0Tn2k5FgPxmGqOR3SnCD16weV8/ZM7B+iWD8sXhi41AXfyVyjejO+wkiRljn5vsYFGYZX18yZm8TSErT1Dn2k+Hl6sJ6FaL+okAFUplCqhmB33eofcL1IKj0XnYP+NsgxYHp3cXUOB53snpySwcz0WvilI7M4np0yOJqga41rnGFZ08dh1AX5Xh4B7LM6zFG3XrePLWwnjcH+8mAALElcY4hmTc9p9x0rv5IJhK/LW8D9r7b4D4KtAZfEButfTmS4XoOACgBnQOQP9A5APkDnQOQP9A5APkDnQOQP011fvHciZXv3nXy0YMqUZ4sthQ6B6DLNNL5S2d/8LOl26784RP/euecSpQnC9mVA3QOQJdppPNffO/uvz5/6L1/Xti98apKlCfL2rG+cmhB59hPZn+pPktlH8ls9ozVjJ4L4k+9hMa5el6AaKTzk0du3Tx313ubw93/vioq/e8K5clCduWQ63o+7/vJ+B/NaETaQ7vSu/0nSverxwnGZ05ppPNnvn37n5+788Ybi5dG92xfvJ9eKU8WsiuHqepcfUCFUvkTKdhPRgqN3FRsqp6uYldng42/ft1O0jk5i2Z92anmyWI60j3H7OHz1U7aYI9kWalDjWmWNWIwJVTWYB8YgTeWGdBI58+euP/0sVs31z+9+58hiZxeKf/0kfc//vX3nTn9LXJoYT0PfC4V+8lIqB3nU5ZctLbXOyNmD2LaF71Kg8HIROV5eAF77Hzj5xLUuaIaP7eI/lmo8fG5KXX+8kvnn3r4Ay8+ddv1jS+Szun1uccP/vzYLbv/vm/liYN/uXB6Njo3srzJ95Mhd+fTps5d+DwriQkxaI+fr4vfrxhrngjFr0Sr8MKsGZ/MaKRzSi/+9uTwa8Vrv/ko6Zxelx4o3n7ls4cOHaLXHz728Y7p3LtXn7XOaZK5I0+T9tBTSZ1/EM/HV0BN7ZAeBDF7Fa4eAe8qJsSAvfZ8jX/ELChVGRO/7E3nofNq+vuVS6Tznzwi0vcfLn702C07G70DBw7svN778Xc+dOLJI7q9Wlpbz2+i/WR0DVlf+vNZ7loW9SK1fCL2ar8icp2VeJrhI8Cp2mV/Ll8+X9lspRY3UCVepRq/Z6DzYN6uqDQ+6nT9E5xrmuqc0ue/9MjrGxsX/vTyNx+8c+Pix3bfvGP3b3fQK+Uffegjur1aJtI59pOpTsBYBXrPEOWud+UusFWcyaCKYnaF3y/vUXRl6/af3Ci3Y+rE7GPGR9ZjhnIzAllcLdC1XPMCr21WqdJnyXPOSdD5PV/4yi9/9TtKD32u2L1+9zuXPvzVBz5Ir5QXlgbsZT1vDvaTyYrYrcEUIZnndgETdP70Mz/t33sfpXs/efuXP1Pw9I0HP6Xbq6UFnas/konEb8vbgK0nGd3vzRC+QGe1ts6CBJ1feu2Vs2d/f+bMC8+urTz/69OUsen48eO6vVr2oHO6X7C3DMgrkFe0mZ9XEnR+7eqbVzffUPkttt8zpWn+vj024sgrkFe0mZ8/EnRek1r4uxoAYGKgcwDyBzoHIH+gcwDyBzoHIH+gcwDyJ6xz8c3H4vEE7/tSjZFo83sUAQB7Jazz5V6xuPpW5fvPhZEyW6sLRevffw4AmJi6+/aSzm0SCzt0DsD8MInOzx89rBZ2m6BzALpMss5J5KWbdkrQOQBdJk3na4tFVeSUoHMAukyCzpd7RbG4Yg95gs4B6DJhnQtJO4Tat9aXPJv/JzfoHIAuU7eeN0/QOQBdBjoHIH+gcwDyBzoHIH+gcwDyBzoHIH+gcwDyBzoHIH+gcwDyBzoHIH/meOt5AEBDoHMAcmd39//G4u0xuty0iAAAAABJRU5ErkJggg==';

        //DicaExibirGlobal.Dica = this.dicaGlobal;

        this.navCtrl.push('DicaPage');
        /*
        var dica:Dica = new Dica();
        dica.IdDica = nome.Id
        dica.IdPartida = nome.Match
        dica.Aluno = new Aluno();

        dica.Aluno.NomeGoogle = localStorage.getItem('Nome');
        dica.Aluno.EmailGoogle = localStorage.getItem('Email');

        
        let body = JSON.stringify(dica);

        var dicaExibir:DicaExibir = new DicaExibir();

        this.http.post('http://10.9.32.8:62015/Api/DicaLog', body, { headers: contentHeaders }).map(
        response => {
          
          return response.json();
        },
        error => {
        }
      ).subscribe(response => {
        //this.dicaGlobal = response;

        DicaExibirGlobal.Dica = response;
        
        this.navCtrl.push('DicaPage');
      },
      error => {
        alert ('Ocorreu um erro ao ler o QR Code!');
      });*/
      }
     }, (err) => {
         // An error occurred
     });
  }

    exit(){    
      this.googlePlus.logout();
      alert ('Logoff efetuado com sucesso!')
      this.navCtrl.pop();
    };
}
