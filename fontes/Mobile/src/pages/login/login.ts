import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GooglePlus } from '@ionic-native/google-plus';
import { LoginGoogle } from '../QRModel';


/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  public a: String;

  constructor(public navCtrl: NavController, public navParams: NavParams, private googlePlus: GooglePlus) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  goHome(){    
    
    this.googlePlus.login({})
    .then(res => {
      var LoginGoogle:LoginGoogle = JSON.parse(JSON.stringify(res));
     
      localStorage.setItem('Nome',LoginGoogle.displayName);
      localStorage.setItem('Email',LoginGoogle.email);

      this.navCtrl.push('MenuPage');
    })          
    .catch(err => alert('Ocorreu um erro ao fazer Login!'));  
    
  }

}
