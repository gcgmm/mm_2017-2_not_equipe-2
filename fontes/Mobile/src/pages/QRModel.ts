export class QRModel {
    public Type;
    public Id;
    public Match;

}

export class Aluno {
    public Id;
    public NomeGoogle;
    public EmailGoogle;
}

export class Login {
    public Id;
    public Aluno:Aluno;
}

export class LoginGoogle {
    public displayName;
    public email;
}

export class Dica {
    public IdDica;
    public IdPartida;
    public Aluno:Aluno;
}

export class DicaExibir {
    public Id;
    public Descricao;
    public Imagem;
    public TipoImagem;
}

export var DicaExibirGlobal = {
    Dica:DicaExibir
}

export var Descricao = {
    Descricao:DicaExibir
}