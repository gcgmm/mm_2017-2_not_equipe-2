import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { QRModel, Login, Aluno, Dica, DicaExibir, DicaExibirGlobal } from '../QRModel';

/**
 * Generated class for the DicaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-dica',
  templateUrl: 'dica.html',
})
export class DicaPage {
  Dica;
  
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.Dica = new DicaExibir();
    this.Dica = DicaExibirGlobal.Dica;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DicaPage');
  }

  getImageBase64(imageType: string, image: string) {
    return 'data: ' + imageType + ';base64,' + image;
  }

}
